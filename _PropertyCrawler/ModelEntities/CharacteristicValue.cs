﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModelEntities
{
  public class CharacteristicValue : IObjectWithState
  {
    public Guid Id { get; set; }

    public long PropertyId { get; set; }
    public Property Property { get; set; }

    public Characteristic Characteristic { get; set; }

    public CustomEntityState EntityState { get; set; }

    public string Value { get; set; }

    public int CharacteristicId { get; set; }
  }
}
