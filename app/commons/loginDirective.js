define(['angularAMD'], function(angularAMD) {
  angularAMD.directive('login', function($timeout) {
    var doLink = function(scope, element, attrs, controller) {
      

      scope.login = function()
      {
        var valid = $(scope.form).valid();
        if(valid)
          {
            alert(scope.userEmail);
          }
        else
        {
            alert('not valid');
        }
      };

      var activeValidation = function() {
        scope.form = $($('#'+scope.formid)[0]);
        $($('#'+scope.formid)[0]).validate({
          rules: {
            userEmail: {
              minlength: 6,
              maxlength: 120,
              required: true
            },
            userPassword: {
              minlength: 6,
              maxlength: 20,
              required: true
            }
          },
          errorPlacement: function(error, element) {
            return true;
          }
        });
      };

      $timeout(activeValidation,0);
      
    };

    var controller = function($scope)
    {
      $scope.userName = null;
      $scope.userEmail = null;
      $scope.userPassword = null;
    }; 


    return {
      restrict: "E",
      replace: true,
      templateUrl: '.../../templates/directives/login.html',
      link: doLink,
      controller: controller,
      scope: {
          formid:'@'
      }
    };
  });
});
