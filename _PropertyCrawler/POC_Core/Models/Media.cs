﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POC_Core.Models
{
  public class Media
  {
    public string ThumbnailTitle { get; set; }
    public string ThumbnailUrl { get; set; }

    public string MediaTitle { get; set; }
    public string MediaUrl { get; set; }

    public MediaType MediaType { get; set; }

    public string Frameborder { get; set; }

    public string Framewidth { get; set; }

    public bool? FrameAllowFullscreen { get; set; }
  }
}
