﻿using POC_Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace POC.SourceProviders
{
  public abstract class SiteProcess : ISiteProcess
  {
    public SiteProcess()
    {
    }
    public bool Initialized { get; set; }
    public bool Finished { get; set; }

    public int ItemPos { get; protected set; }

    public List<HtmlElement> Items { get; set; }


    protected abstract void Process(ISiteProcessor siteProcessor);

    protected abstract void Initialize(ISiteProcessor siteProcessor);

    public void ProcessCall(ISiteProcessor siteProcessor, string url)
    {

      if (!this.Initialized)
      {
        Initialize(siteProcessor);
        this.Initialized = true;
      }

      Process(siteProcessor);
    }

    public virtual Property SelectProperty(ISiteProcessor siteProcessor, string content)
    {
      string idType;
      var id = siteProcessor.InfoParser.IdFromListItem(content, out idType);

      var property = siteProcessor.Properties.FirstOrDefault(item => item.InternalId == id);
      if (property == null)
      {
        property = new Property { InternalId = id, IdType = idType };
        siteProcessor.Properties.Add(property);
      }
      else if (property.IdType == null)
      {
        property.IdType = idType;
      }
      return property;
    }
  }

}
