﻿using ModelEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkModeling
{
  public class BaseContext<T>: DbContext where T: DbContext
  {

    public BaseContext() : base("PropertiesConnectionString") {
      Database.SetInitializer<T>(null);
    }

  }
}
