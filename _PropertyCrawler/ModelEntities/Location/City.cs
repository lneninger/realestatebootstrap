﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelEntities.Location
{
  public class City
  {
    public int Id { get; set; }
    public State State { get; set; }
    public int StateId { get; set; }
    public string Identifier { get; set; }
  }
}
