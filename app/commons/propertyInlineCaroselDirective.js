define(['angularAMD'], function(angularAMD) {
  angularAMD.directive('propertyInlineCarosel', ['$q', '$location', 'propertyTypeService', 'propertyCharacteristicService', 'propertyCharacteristicCategoryService',
    function($q, $location, propertyTypeService, propertyCharacteristicService, propertyCharacteristicCategoryService) {
      var doLink = function(scope, element, attrs, controller) {
        var property = scope.selected;





        //scope.detailedCharacteristics = getDetailedCharacteristics(scope.selected);

        scope.showTypeName = function() {
          return (scope.selected != null && scope.selected.propertyType != null) ? scope.selected.propertyType.nm_identifier : null;
        };

        $('#propertyDetails').affix({
          offset: {
            top: 400,
            bottom: 400

          }
        });

      };



      var controller = function($scope) {

        var iscope = $scope;

        // Use the old watch() with default object equality,
        // which defaults to use object REFERENCE checks.
        $scope.$watch(
          "selected",
          function(newValue, oldValue) {
            $scope.detailedCategories = getDetailedCategories(newValue);
          }
        );

        var getDetailedCategories = function(selected) {
          var detailedCategories = [];
          if (selected == null) {
            return detailedCategories;
          }

          $q.all([propertyTypeService.query(), propertyCharacteristicService.query(), propertyCharacteristicCategoryService.query()])
            .then(function(results) {

              var propertyTypes = results[0];
              var propertyCharacteristics = results[1];
              var propertyCharacteristicCategories = results[2];

              var propertyType = propertyTypes.first(function(i) {
                return i.cd_propertyType == $scope.selected.propertyType.cd_propertyType;
              });

              var orderedCharacteristics = propertyType.characteristics.orderBy(function(chr) {
                return chr.nr_detail_order;
              });

              var detailedCategories = [];

              for (var i = 0; i < orderedCharacteristics.length; i++) {
                var tempCharacteristic = orderedCharacteristics[i];
                var originCharacteristic = propertyCharacteristics.first(function(item) {
                  return item.cd_propertyCharacteristic == tempCharacteristic.cd_propertyCharacteristic
                });

                var propertyValue = selected.propertyValues.first(function(item) {
                  return item.cd_propertyCharacteristic == tempCharacteristic.cd_propertyCharacteristic
                });

                var associatedCategory = propertyCharacteristicCategories.first(function(item){
                  return item.characteristics.indexOf(originCharacteristic.cd_propertyCharacteristic) > -1;
                });



                if (propertyValue != null) {
                  var resultCategory = detailedCategories[associatedCategory.cd_characteristicCategory];
                  if(resultCategory == undefined)
                  {
                    resultCategory = $.extend(true, {}, associatedCategory);
                    resultCategory.characteristics = [];
                    detailedCategories[associatedCategory.cd_characteristicCategory] = resultCategory;
                  }

                  resultCategory.characteristics.push({
                    cd_propertyCharacteristic: originCharacteristic.cd_propertyCharacteristic,
                    nm_identifier: originCharacteristic.nm_identifier,
                    ds_value: propertyValue != null ? propertyValue.ds_value : null
                  });
                }
              }

              $scope.detailedCategories = detailedCategories;

            });
        };

        $scope.detailedCategories = getDetailedCategories($scope.selected);


      };

      return {
        restrict: "E",
        replace: true,
        templateUrl: '.../../templates/directives/propertyInlineCarosel.html',
        link: doLink,
        controller: controller,
        scope: {
          'selected': '=',

        }
      };
    }
  ]);
});
