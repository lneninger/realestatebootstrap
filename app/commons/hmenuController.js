define(['app'], function(myApp) {
  myApp.controller('hmenuController', ['$scope', '$location',
    function($scope, $location) {
      $scope.isActive = function(route) {
        return route === $location.path();
      }
    }
  ]);
});
