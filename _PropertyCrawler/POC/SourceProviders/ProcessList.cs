﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POC.SourceProviders
{
  public class ProcessList : List<ISiteProcess>
  {
    public int ItemPos { get; set; }
    public Stack<SiteProcessItem> ReferedUrlStack { get; set; }

    public ProcessList()
    {
      this.ReferedUrlStack = new Stack<SiteProcessItem>();
    }

    public ProcessList AddProcess(ISiteProcess process)
    {
      base.Add(process);
      return this;
    }

    public bool Finished
    {
      get
      {
        return this.All(item => item.Finished);
      }
    }

    internal void Process(ISiteProcessor siteProcessor, string url)
    {
      if (this[ItemPos].Finished)
      {
        ItemPos++;
      }

      if (ItemPos >= this.Count)
      {
        siteProcessor.UrlHistory.Remove(siteProcessor.UrlHistory.Last());
        var refered = ReferedUrlStack.Pop();
        siteProcessor.WebBrowser.Navigate(refered.Url);
      }
      else
      {
        if (siteProcessor.UrlHistory.Count == 0 || siteProcessor.UrlHistory.Last() != url)
        {
          siteProcessor.UrlHistory.Add(url);
        }
        this[ItemPos].ProcessCall(siteProcessor, url);
      }

      if (this[ItemPos].Finished)
      {
        if (ItemPos < this.Count - 1)
        {
          ItemPos++;
          Process(siteProcessor, url);
        }
        else
        {
          siteProcessor.UrlHistory.Remove(siteProcessor.UrlHistory.Last());
          siteProcessor.WebBrowser.GoBack();
        }
      }
    }
  }
}
