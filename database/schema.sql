create database RealEstate;
GO

create schema Properties
GO

create schema PropertyDomains
GO

create schema PropertyPendings
GO

create table Properties.TB_UserMessages
(
	CD_UserMessage bigint identity,
	DS_UserEmail varchar(200),
	DS_Message varchar(1024),
	DT_Created datetime,
	DS_CellPhone varchar(20),
	DS_ResidencialPhone varchar(20),
	constraint PK_UserMessage primary key(CD_UserMessage)
)

create table PropertyDomains.[Types]
(
	CD_Type int, NM_Identifier varchar(32),
	constraint PK_Type primary key(CD_Type)
)

create table PropertyDomains.PropertyStatuses
(
	CD_PropertyStatus bigint, NM_Identifier varchar(32),
	constraint PK_PropertyStatus primary key(CD_PropertyStatus),
)	


create table Properties.Properties
(
	CD_Property bigint identity,
	CD_Owner int,
	CD_PropertyType int,
	DT_Created datetime,
	DT_LastUpdate datetime,
	constraint PK_Property primary key(CD_Property),
	constraint FK_Property_PropertyType foreign key (CD_PropertyType) references Properties.TB_PropertyTypes(CD_PropertyType)
)

create table Properties.TB_PropertyHistoryLog
(
	CD_PropertyHistoryLog bigint identity,
	CD_Property bigint,
	CD_PropertyStatus int,
	DT_Created datetime,
	constraint PK_PropertyHistoryLog primary key(CD_Property)
)	

create table Properties.TB_MediaTypes
(
	CD_MediaType int not null,
	NM_Identifier varchar(32),
	constraint PK_MediaType primary key(CD_MediaType)
)

create table Properties.TB_PropertyMedias
(
	CD_PropertyMedia bigint identity,
	CD_Property bigint
	DS_URL varchar(1024),
	DS_SystemPath varchar(1024),
	CD_MediaType int,
	constraint PK_PropertyMedia primary key(CD_PropertyMedia),
	constraint FK_Property foreign key(CD_Property) references Properties.TB_Property(CD_Property),
	constraint FK_MediaType foreign key(CD_MediaType) references Properties.TB_MediaTypes(CD_MediaType)
)

create table Properties.TB_PropertyCategories
(
	CD_PropertyCategory int,
	NM_Identifier int,
	constraint PK_PropertyCategory primary key(CD_PropertyCategory)
)

create table Properties.TB_PropertyCharacteristics
(
	CD_PropertyCharacteristic int not null,
	CD_PropertyCategory int,
	NM_Identifier int,
	NM_DataType varchar(16),
	constraint PK_PropertyCharacteristic primary key(CD_PropertyCharacteristic),
	constraint FK_Property_PropertyCategory foreign key (CD_PropertyCategory) references Properties.TB_PropertyCategories(CD_PropertyCategory)
)	

create table Properties.TB_PropertyCharacteristicValues
(
	CD_PropertyValue bigint identity,
	CD_Property bigint,
	CD_PropertyCharacteristic int,
	DS_Value varchar(1024),
	constraint PK_PropertyCharacteristicValue primary key(CD_PropertyCharacteristicValue),
	constraint FK_Property_Property foreign key (CD_Property) references TB_Properties(CD_Property)
	constraint FK_Property_PropertyCharacteristic foreign key (CD_PropertyCharacteristic) references Properties.TB_PropertyCharacteristics(CD_PropertyCharacteristic)
)	