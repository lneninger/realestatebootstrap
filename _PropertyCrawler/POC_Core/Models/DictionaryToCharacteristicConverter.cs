﻿using AutoMapper;
using ModelEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POC_Core.Models
{
  public class DictionaryToCharacteristicConverter: ITypeConverter<Dictionary<string, string>, List<CharacteristicValue>>
  {
    public List<CharacteristicValue> Convert(ResolutionContext context)
    {

      var source = (Dictionary<string, string>)context.SourceValue;
      var result = new List<CharacteristicValue>();
      foreach (var keyvalue in source)
      {
        var name = keyvalue.Key;
        var value = keyvalue.Value;

        var characteristicIdentifier = string.Empty;
      }

      return result;
    }
  }
}
