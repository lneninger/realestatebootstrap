﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using POC_Core.Models;
using Newtonsoft.Json.Linq;
using System.IO;
using EntityFrameworkModeling;
using AutoMapper;
using EntityFrameworkModeling.Repositories;
using ModelEntities;
using PropertyContext;

namespace POC.SourceProviders.VivaReal
{
  public class SiteProcessor : ISiteProcessor
  {
    public SiteProcessor()
    {
      this.InfoParser = new InfoParser();
      this.UrlProcesses = new Dictionary<string, ProcessList>();
      this.UrlHistory = new List<string>();
      this.Properties = new List<POC_Core.Models.Property>();
    }

    public WebBrowser WebBrowser { get; set; }
    public IInfoParser InfoParser { get; set; }
    public Dictionary<string, ProcessList> UrlProcesses { get; protected set; }
    public List<string> UrlHistory { get; set; }
    public List<POC_Core.Models.Property> Properties { get; protected set; }
    protected int DetailCurrentPropertyIndex { get; set; }

    string startUrl = "http://www.vivareal.com.br";
    bool Initialized { get; set; }


    public void Process()
    {
      if (!Initialized)
      {
        Initialized = true;
      }

      ProcessSite();
    }

    private void ProcessSite()
    {

      var url = string.Empty;
      var counter = 0;
      var propertiesRow = new List<POC_Core.Models.Property>();
      for (var i = 0; i < BaseUrls.states.Length; i++)
      {
        for (var j = 0; j < BaseUrls.cities.Length; j++)
        {
          for (var k = 0; j < BaseUrls.ListUrls.Length; k++)
          {
            var finished = false;
            counter = 1;
            do
            {
              url = BaseUrls.ListUrls[k].Replace("<state>", BaseUrls.states[i]).Replace("<city>", BaseUrls.cities[j]).Replace("<page>", counter.ToString());
              JToken jsonObj = null;
              var response = HttpRequest(url);

              try
              {
                jsonObj = JToken.Parse(response);
              }
              catch (Exception ex)
              {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                break;
              }

              var propertyArray = jsonObj.Value<JArray>("listings");
              for (var l = 0; l < propertyArray.Count; l++)
              {
                var propertyObj = propertyArray[l];
                var propertyRow = new POC_Core.Models.Property();
                propertyObj.Children().ToList().ForEach(item =>
                {
                  TokenParser(propertyRow, item);
                });
                propertiesRow.Add(propertyRow);
              }
              List<ModelEntities.Property> properties = new List<ModelEntities.Property>();
              ModelEntities.Property property;
              foreach (var propertyRowItem in propertiesRow)
              {
                property = Mapper.Map<ModelEntities.Property>(propertyRowItem);

                property.EntityState = CustomEntityState.Added;
                property.CharacteristicValues = this.InfoParser.ConvertToCharacteristics(propertyRowItem.Attributes);

                if (!VerifyPropertyInDb(property))
                {
                  continue;
                }

                property.CharacteristicValues.ToList().ForEach(characteristicItem =>
                {
                  if (characteristicItem.Characteristic != null && characteristicItem.Characteristic is CharacteristicNotMapped)
                  {
                    (characteristicItem.Characteristic as CharacteristicNotMapped).EntityState = CustomEntityState.Added;
                  }
                });

                var pendingMedias = new List<MediaPendingDownload>();
                foreach (var media in property.Medias)
                {
                  media.Property = property;
                  var pendingMedia = Mapper.Map<MediaPendingDownload>(media);
                  pendingMedia.EntityState = CustomEntityState.Added;
                  pendingMedias.Add(pendingMedia);
                }
                property.Medias.Clear();

                property.CharacteristicValues.ToList().ForEach(characteristicItem =>
                {
                  if (characteristicItem.Characteristic != null && characteristicItem.Characteristic is CharacteristicNotMapped)
                  {
                    (characteristicItem.Characteristic as CharacteristicNotMapped).EntityState = CustomEntityState.Added;
                  }
                });

                var propertyContext = new PropertyContextClass();
                using (var repository = new PropertyRepository(propertyContext))
                {
                  repository.InsertOrUpdate(property);
                  var objWithState = propertyContext.ChangeTracker.Entries<IObjectWithState>();
                  repository.SaveChanges();
                }

                using (var repository = new MediaPendingDownloadRepository(new PropertyContextClass()))
                {
                  repository.InsertOrUpdate(pendingMedias.ToArray());
                  repository.SaveChanges();
                }
              }
              counter++;
              var test1 = response;
            }
            while (!finished);
          }
        }
      }
    }

    private bool VerifyPropertyInDb(ModelEntities.Property property)
    {
      var addProperty = false;
      using (var repository = new PropertyRepository(new PropertyContextClass()))
      {
        addProperty = !repository.AllIncluding().Any(item => item.ExternalId == property.ExternalId && item.PropertySource == PropertySourcesEnum.VivaReal);
      }

      return addProperty;
    }

    private static string HttpRequest(string url)
    {
      var stream = new MemoryStream();
      var request = System.Net.WebRequest.Create(url);
      using (var response = request.GetResponse())
      {
        response.GetResponseStream().CopyTo(stream);
      }
      var test = System.Text.Encoding.UTF8.GetString(stream.GetBuffer());
      return test;
    }

    public static string[] externalNoCharacteristicNames = new string[] { "links", "price", "propertyId", "image", "thumbnail", "thumbnails", "images", "developmentInformation", "additionalFeatures" };
    private static void TokenParser(POC_Core.Models.Property property, JToken item)
    {
      var jProperty = item as JProperty;
      if (externalNoCharacteristicNames.Contains(jProperty.Name))
      {
        property.SetValue(jProperty);
      }
      else
      {
        property.Attributes.Add(jProperty.Name, jProperty.Value.Value<string>());
      }
    }
  }

  public static class PropertyExtensions
  {
    public static void SetValue(this POC_Core.Models.Property property, JProperty jProperty)
    {
      if (jProperty.Name == "propertyId")
      {
        property.InternalId = jProperty.Value.Value<string>();
      }

      if (jProperty.Name == "thumbnail")
      {
        var thumbnail = jProperty.Value.Value<string>();
        if (property.Thumbnails.Count == 0 || (property.Thumbnails[0].ThumbnailUrl != null && property.Thumbnails[0].MediaUrl != null))
        {
          property.Thumbnails.Insert(0, new POC_Core.Models.Media());
        }
        property.Thumbnails[0].ThumbnailTitle = null;
        property.Thumbnails[0].ThumbnailUrl = thumbnail;
      }

      if (jProperty.Name == "image")
      {
        var url = jProperty.Value.Value<string>();
        if (property.Thumbnails.Count == 0 || (property.Thumbnails[0].ThumbnailUrl != null && property.Thumbnails[0].MediaUrl != null))
        {
          property.Thumbnails.Insert(0, new POC_Core.Models.Media());
        }
        property.Thumbnails[0].MediaTitle = null;
        property.Thumbnails[0].MediaUrl = url;
      }

      if (jProperty.Name == "thumbnails")
      {
        var jArray = jProperty.Value.Value<JArray>();
        for (var i = 0; i < jArray.Count; i++)
        {
          var thumbnail = jArray[i];
          if (property.Thumbnails.Count <= i)
          {
            property.Thumbnails.Add(new POC_Core.Models.Media());
          }
          property.Thumbnails[i].ThumbnailTitle = null;
          property.Thumbnails[i].ThumbnailUrl = thumbnail.Value<string>();
        }

      }

      if (jProperty.Name == "images")
      {
        var jArray = jProperty.Value.Value<JArray>();
        for (var i = 0; i < jArray.Count; i++)
        {
          var jToken = jArray[i];
          if (property.Thumbnails.Count < i - 1)
          {
            property.Thumbnails.Add(new POC_Core.Models.Media());
          }
          property.Thumbnails[i].MediaTitle = null;
          property.Thumbnails[i].MediaUrl = jToken.Value<string>();
        }

      }

      if (jProperty.Name == "additionalFeatures")
      {
        var jArray = jProperty.Value.Value<JArray>();
        if (jArray == null)
          return;
        for (var i = 0; i < jArray.Count; i++)
        {
          var jToken = jArray[i];
          property.Attributes.Add(jToken.Value<string>(), true.ToString());
        }

      }

      if (jProperty.Name == "links")
      {
        var jArray = jProperty.Value.Value<JArray>();
        if (jArray == null)
          return;
        for (var i = 0; i < jArray.Count; i++)
        {
          var jToken = jArray[i];
          property.Attributes.Add(jToken.Value<string>("rel"), jToken.Value<string>("href"));
        }

      }

    }

  }


}
