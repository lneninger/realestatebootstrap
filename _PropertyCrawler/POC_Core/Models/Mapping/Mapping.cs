﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POC_Core.Models
{
  public static class Mapping
  {
    public static void Map() {
      PropertyMapping();
      CharacteristicMapping();
      PropertyMediaMapping();

      Mapper.CreateMap<POC_Core.Models.Media, ModelEntities.Media>()
        .ForMember(dest => dest.MediaTitle, opt => opt.MapFrom(src => src.MediaTitle))
        .ForMember(dest => dest.MediaUrl, opt => opt.MapFrom(src => src.MediaUrl))
        .ForMember(dest => dest.ThumbnailTitle, opt => opt.MapFrom(src => src.ThumbnailTitle))
        .ForMember(dest => dest.ThumbnailUrl, opt => opt.MapFrom(src => src.ThumbnailUrl));

      Mapper.CreateMap<POC_Core.Models.Media, ModelEntities.MediaPendingDownload>()
        .ForMember(dest => dest.MediaUrl, opt => opt.MapFrom(src => src.MediaUrl))
        .ForMember(dest => dest.ThumbnailUrl, opt => opt.MapFrom(src => src.ThumbnailUrl));
    }

    private static void PropertyMediaMapping()
    {
      Mapper.CreateMap<ModelEntities.Media, ModelEntities.MediaPendingDownload>()
        .ForMember(dest => dest.Property, opt => opt.MapFrom(src => src.Property))
        .ForMember(dest => dest.MediaUrl, opt => opt.MapFrom(src => src.MediaUrl))
        .ForMember(dest => dest.ThumbnailUrl, opt => opt.MapFrom(src => src.ThumbnailUrl))
        .ForMember(dest => dest.Property, opt => opt.MapFrom(src => src.Property));
    }

    private static void PropertyMapping()
    {
      Mapper.CreateMap<POC_Core.Models.Property, ModelEntities.Property>()
        .ForMember(dest => dest.ExternalId, opt => opt.MapFrom(src => src.InternalId))
        .ForMember(dest => dest.Medias, opt => opt.MapFrom(src => src.Thumbnails))
        /*.ForMember(dest => dest.CharacteristicValues, opt => opt.MapFrom(src => src.Attributes))*/;
    }


    private static void CharacteristicMapping()
    {
      Mapper.CreateMap<Dictionary<string, string>, List<ModelEntities.CharacteristicValue>>().ConvertUsing<DictionaryToCharacteristicConverter>();
    }
  }
}
