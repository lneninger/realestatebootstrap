﻿using ModelEntities;
using PropertyContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkModeling.Repositories
{
  public class PropertyRepository:BaseRepository<Property>
  {
    public PropertyRepository(DbContext context) : base(context) { }
    public PropertyRepository() : base(new PropertyContextClass()) { }
  }
}
