define(['angularAMD'], function(angularAMD) {
  angularAMD.directive('propertyFilter', ['addressService', '$timeout', 'toaster',
    function(addressService, $timeout, toaster) {
      var doLink = function(scope, element, attrs, controller) {

        scope.loadPresentation();

        scope.addressSelection = function(selection) {
          if (!scope.stateTemp) {
            scope.stateTemp = selection;
          } else if (!scope.cityTemp) {
            scope.cityTemp = selection;
          } else {
            scope.neigborhoodTemp.push(selection);
          }

          scope.loadPresentation();
        };

      };


      var controller = function($scope) {
        $scope.withPicture = true;

        $scope.closeLocationSelecion = function() {
          $scope.stateTemp = $.extend(true, {}, $scope.state);
          $scope.cityTemp = $.extend(true, {}, $scope.city);
          $scope.neigborhoodTemp = $.extend(true, [], $scope.neigborhood);
        };

        $scope.saveLocationSelection = function() {
          $scope.state = $.extend(true, {}, $scope.stateTemp);
          $scope.city = $.extend(true, {}, $scope.cityTemp);
          $scope.neigborhood = $.extend(true, [], $scope.neigborhoodTemp);
        };

        $scope.loadPresentation = function() {
          var data = null;

          addressService.query(function(result) {
            if (!$scope.stateTemp) {
              data = result.select(function(item) {
                return {
                  id: item.cd_state,
                  name: item.nm_identifier
                };
              });
            } else if (!$scope.cityTemp) {
              data = result
                .first(function(item) {
                  return item.cd_state == $scope.stateTemp.id;
                })
                .cities.select(function(item) {
                  return {
                    id: item.cd_city,
                    name: item.nm_identifier
                  };
                });
            } else {
              data = result
                .first(function(item) {
                  return item.cd_state == $scope.stateTemp.id;
                })
                .cities.first(function(item) {
                  return item.cd_city == $scope.cityTemp.id;
                })
                .neigborhoods.select(function(item) {
                  return {
                    id: item.cd_neigborhood,
                    name: item.nm_identifier
                  };
                });
            }

            $scope.internalCurrentPresentation = $scope.locationCurrentPresentation = data;
          });
        };



        $scope.$watch('locationSearchTerm', function(newValue, oldValue) {
          if (newValue && newValue.length > 0) {
            $scope.locationCurrentPresentation = $scope.internalCurrentPresentation.where(function(item) {
              return item.name.toLowerCase().indexOf($scope.locationSearchTerm.toLowerCase()) > -1;
            });
          } else {
            $scope.locationCurrentPresentation = $scope.internalCurrentPresentation;
          }
        });

        var timeoutPromise = null;
        $scope.$watchGroup(['state', 'city', 'neigborhood', 'current', 'current.minPrice', 'current.maxPrice', 'current.minRooms', 'current.maxRooms', 'current.minSuites', 'current.maxSuites', 'current.minGarage', 'current.maxGarage', 'current.minArea', 'current.maxArea'],
          function(newValue, oldValue) {
            if (timeoutPromise) {
              $timeout.cancel(timeoutPromise);
            }
            timeoutPromise = $timeout(function() {
              if ($scope.visible) {
                toaster.pop('success', "Filtro mudado", "Nova pesquisa baseada na nova seleçao");
              }
            }, 2000);
          });

        $scope.state = null;
        $scope.city = null;
        $scope.neigborhood = [];

        $scope.stateTemp = null;
        $scope.cityTemp = null;
        $scope.neigborhoodTemp = [];

        $scope.locationSearchTerm = null;
        $scope.locationCurrentPresentation = [];
        $scope.internalCurrentPresentation = [];


        $scope.absolute = {
          minPrice: 0,
          maxPrice: 60,

          minRooms: 0,
          maxRooms: 7,

          minSuites: 0,
          maxSuites: 4,

          minGarage: 0,
          maxGarage: 4,

          minArea: 0,
          maxArea: 200
        };
        $scope.current = {
          minRooms: $scope.absolute.minRooms,
          maxRooms: $scope.absolute.maxRooms,

          minSuites: $scope.absolute.minSuites,
          maxSuites: $scope.absolute.maxSuites,

          minPrice: $scope.absolute.minPrice,
          maxPrice: $scope.absolute.maxPrice,

          minGarage: $scope.absolute.minGarage,
          maxGarage: $scope.absolute.maxGarage,

          minArea: $scope.absolute.minArea,
          maxArea: $scope.absolute.maxArea
        };

        $scope.currencyFormatting = function(value) {
          if (value != undefined) {
            return value.value.toString() + " $";
          }
          return value;
        };


      };

      return {
        restrict: "E",
        templateUrl: '.../../templates/directives/propertyFilter.html',
        link: doLink,
        controller: controller,
        scope: {
          visible: "=ngShow"
        }

      };
    }
  ]);

});
