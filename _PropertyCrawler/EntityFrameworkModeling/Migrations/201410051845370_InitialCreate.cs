namespace EntityFrameworkModeling.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Properties.Properties",
                c => new
                    {
                        CD_Property = c.Long(nullable: false, identity: true),
                        PropertySource = c.Int(nullable: false),
                        NM_ExternalId = c.String(maxLength: 2000),
                        EntityState = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CD_Property);
            
            CreateTable(
                "Properties.CharacteristicValues",
                c => new
                    {
                        CD_CharacteristicValue = c.Guid(nullable: false),
                        EntityState = c.Int(nullable: false),
                        Value = c.String(),
                        CharacteristicId = c.Int(nullable: false),
                        Property_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.CD_CharacteristicValue)
                .ForeignKey("dbo.Characteristics", t => t.CharacteristicId, cascadeDelete: true)
                .ForeignKey("Properties.Properties", t => t.Property_Id, cascadeDelete: true)
                .Index(t => t.CharacteristicId)
                .Index(t => t.Property_Id);
            
            CreateTable(
                "dbo.Characteristics",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Identifier = c.String(),
                        DataType = c.String(),
                        CategoryId = c.Int(nullable: false),
                        PropertySource = c.Int(),
                        EntityState = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Identifier = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TypeCharacteristics",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Order = c.Int(nullable: false),
                        CharacteristicId = c.Int(nullable: false),
                        TypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Characteristics", t => t.CharacteristicId, cascadeDelete: true)
                .ForeignKey("dbo.Types", t => t.TypeId, cascadeDelete: true)
                .Index(t => t.CharacteristicId)
                .Index(t => t.TypeId);
            
            CreateTable(
                "dbo.Types",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Identifier = c.String(),
                        Characteristic_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Characteristics", t => t.Characteristic_Id)
                .Index(t => t.Characteristic_Id);
            
            CreateTable(
                "Properties.PropertyMedia",
                c => new
                    {
                        CD_Media = c.Guid(nullable: false, identity: true),
                        CD_Property = c.Long(nullable: false),
                        NM_MediaTitle = c.String(maxLength: 200),
                        MediaUrl = c.String(),
                        NM_ThumbnailTitle = c.String(maxLength: 200),
                        DS_MediaUrl = c.String(maxLength: 2000),
                        IN_Active = c.Boolean(nullable: false),
                        EntityState = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CD_Media)
                .ForeignKey("Properties.Properties", t => t.CD_Property, cascadeDelete: true)
                .Index(t => t.CD_Property);
            
            CreateTable(
                "Pendings.PropertyMediaDownload",
                c => new
                    {
                        CD_Media = c.Guid(nullable: false, identity: true),
                        MediaUrl = c.String(),
                        DS_MediaUrl = c.String(maxLength: 2000),
                        CD_Property = c.Long(nullable: false),
                        EntityState = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CD_Media)
                .ForeignKey("Properties.Properties", t => t.CD_Property, cascadeDelete: true)
                .Index(t => t.CD_Property);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Pendings.PropertyMediaDownload", "CD_Property", "Properties.Properties");
            DropForeignKey("Properties.PropertyMedia", "CD_Property", "Properties.Properties");
            DropForeignKey("Properties.CharacteristicValues", "Property_Id", "Properties.Properties");
            DropForeignKey("Properties.CharacteristicValues", "CharacteristicId", "dbo.Characteristics");
            DropForeignKey("dbo.Types", "Characteristic_Id", "dbo.Characteristics");
            DropForeignKey("dbo.TypeCharacteristics", "TypeId", "dbo.Types");
            DropForeignKey("dbo.TypeCharacteristics", "CharacteristicId", "dbo.Characteristics");
            DropForeignKey("dbo.Characteristics", "CategoryId", "dbo.Categories");
            DropIndex("Pendings.PropertyMediaDownload", new[] { "CD_Property" });
            DropIndex("Properties.PropertyMedia", new[] { "CD_Property" });
            DropIndex("dbo.Types", new[] { "Characteristic_Id" });
            DropIndex("dbo.TypeCharacteristics", new[] { "TypeId" });
            DropIndex("dbo.TypeCharacteristics", new[] { "CharacteristicId" });
            DropIndex("dbo.Characteristics", new[] { "CategoryId" });
            DropIndex("Properties.CharacteristicValues", new[] { "Property_Id" });
            DropIndex("Properties.CharacteristicValues", new[] { "CharacteristicId" });
            DropTable("Pendings.PropertyMediaDownload");
            DropTable("Properties.PropertyMedia");
            DropTable("dbo.Types");
            DropTable("dbo.TypeCharacteristics");
            DropTable("dbo.Categories");
            DropTable("dbo.Characteristics");
            DropTable("Properties.CharacteristicValues");
            DropTable("Properties.Properties");
        }
    }
}
