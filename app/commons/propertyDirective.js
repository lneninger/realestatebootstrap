define(['app'], function(myApp) {
  myApp.directive('propertyItem', ['$q', '$compile', 'propertyTypeService',
    function($q, $compile, propertyTypeService) {
      var doLink = function(scope, element, attrs, controller) {

        var property = scope.property;

        element.on({
          mouseenter: function() {
            var options = $('[data-property-options]').clone();
            options = $('<p/>').append(options).html();
            var options = $compile(options)(scope);

            options.show();
            options.insertBefore(element.find('img.thumbnail'));
          },
          mouseleave: function() {
            element.find('[data-property-options]').remove();
          }
        });

        scope.viewDetails = function() {
          scope.showDetails();
        };

        scope.$watch(element, function(value) {

          var propertyTypes = propertyTypeService.query();

          $q.all([
            propertyTypeService.query()
          ]).then(function(propertyTypes) {

            var propertyType = propertyTypes[0].first(function(i) {
              return i.cd_propertyType == scope.property.propertyType.cd_propertyType;
            });

			scope.property.propertyType.nm_identifier = propertyType.nm_identifier;

            domTooltipBinder(element.find('[data-toggle="tooltip"],[data-tooltip]'));
            element.attr('id', scope);
            element.find('h5.bold').text(scope.property.propertyType.nm_identifier);
          });


        });
      };

      return {
        restrict: "AE",
        replace: true,
        templateUrl: '.../../templates/directives/property.html',
        link: doLink,
        scope: {
          showDetails: '&',
          property: '='
        }
      };
    }
  ]);
});
