﻿using ModelEntities;
using PropertyContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkModeling.Repositories
{
  public class MediaPendingDownloadRepository:BaseRepository<MediaPendingDownload>
  {
    public MediaPendingDownloadRepository(DbContext context) : base(context) { }
    public MediaPendingDownloadRepository() : base(new PropertyContextClass()) { }
  }
}
