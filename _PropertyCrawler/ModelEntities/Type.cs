﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelEntities
{
  public class Type
  {
    public int Id { get; set; }

    public string Identifier { get; set; }

    public ICollection<TypeCharacteristic> TypeCharacteristics { get; set; }
  }
}
