﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using POC.SourceProviders;
using POC_Core.Models;

namespace POC
{
  class Program
  {
    static void Main(string[] args)
    {
      Mapping.Map();
      var test = new Program();
      var browser = new Browser(Processors.VivaReal, test.BrowserNavigate);
      browser.RunBrowserThread();
    }

   

    public object BrowserNavigate(Browser browser)
    {
      if (browser.SiteProcessor.WebBrowser.ReadyState != WebBrowserReadyState.Complete)
        return null;

      Console.WriteLine(browser.SiteProcessor.WebBrowser.Url);
      browser.SiteProcessor.Process();
      return null;
    }
  }

  public class Browser
  {
    Func<Browser, object> callBack;
    public ISiteProcessor SiteProcessor;
    public Browser(ISiteProcessor siteProcessor, Func<Browser, object> callBack)
    {
      this.callBack = callBack;
      this.SiteProcessor = siteProcessor;
    }


    public void RunBrowserThread()
    {
      var th = new Thread(() =>
      {
        SiteProcessor.WebBrowser = new WebBrowser();
        SiteProcessor.WebBrowser.DocumentCompleted += browser_DocumentCompleted;
        SiteProcessor.WebBrowser.Navigate("http://www.google.com");
        Application.Run();
      });
      th.SetApartmentState(ApartmentState.STA);
      th.Start();
    }

    private void browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
    {
      var browser = sender as WebBrowser;
      callBack(this);
    }


  }
}
