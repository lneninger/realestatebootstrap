﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EntityFrameworkModeling.Repositories
{
  public class BaseRepository<T>: IBaseRepository<T> where T: class
  {
    public DbContext context { get; set; }

    public BaseRepository(DbContext context)
    {
      this.context = context;
    }

    public IQueryable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties)
    {
      var query = context.Set<T>().AsQueryable();
      foreach(var includeProperty in includeProperties)
      {
        query.Include(includeProperty);
      }

      return query;
    }

    public void Delete<TId>(TId id)
    {
      var entity = context.Set<T>().Find(id);
      context.Entry(entity).State = EntityState.Deleted;
    }
    public void InsertOrUpdate(params T[] entities)
    {
      foreach (var entity in entities)
      {
        this.context.Entry(entity).State = EntityState.Added;
      }
    }

    public void SaveChanges()
    {
      this.context.SetCurrentStateToEntities();
      this.context.SaveChanges();
      this.context.SetToUndefined();
    }

    public T Find<TId>(TId id)
    {
      return this.context.Set<T>().Find(id);
    }

    public void Dispose()
    {
      this.context.Dispose();
    }
  }
}
