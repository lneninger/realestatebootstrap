﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using EntityFrameworkModeling;
using EntityFrameworkModeling.Repositories;
using ModelEntities;
using POC_Core.Models;
using PropertyContext;

namespace POC.SourceProviders.VivaReal
{
  public class InfoParser : IInfoParser
  {
    #region Old parsing
    public string Price(string content, out string currency)
    {
      var match = Regex.Match(content, @"<SMALL[\s]*class=lu_p_currency>(?<currency>[\S]*)\s+</SMALL><SPAN>(?<price>[\d.,]+)</SPAN>", RegexOptions.Multiline | RegexOptions.IgnoreCase);
      //var priceMatch = Regex.Match(content, @"(?<currency>[\S]*)\s+(?<price>[\d.,]+)", RegexOptions.None);
      if (match.Success)
      {
        currency = match.Groups["currency"].Value;
        return match.Groups["price"].Value;
      }

      match = Regex.Match(content, @"<span\s+itemprop=""price"">(?<currency>[^\s]+)[\s]*(?<price>[\d.]+)</span>", RegexOptions.Multiline | RegexOptions.IgnoreCase);
      if (match.Success)
      {
        currency = match.Groups["currency"].Value;
        return match.Groups["price"].Value;
      }

      currency = Constants.NotAvailable;
      return Constants.NotAvailable;
    }

    public string StartupFromListItem(string content)
    {
      var match = Regex.Match(content, @"/imove(l|is)-(?<step>[^\/]*)/", RegexOptions.Multiline | RegexOptions.IgnoreCase);
      if (match.Success)
      {
        return match.Groups["step"].Value.ToUpper();
      }

      return Constants.NotAvailable;
    }

    public string TypeFromListItem(string content)
    {
      var match = Regex.Match(content, @"/imove(l|is)(-)?(?<step>[^\/]*)?/(?<type>[^-]*)", RegexOptions.Multiline | RegexOptions.IgnoreCase);
      if (match.Success)
      {
        return match.Groups["type"].Value.ToUpper();
      }

      return Constants.NotAvailable;
    }


    public string IdFromListItem(string content, out string idType)
    {
      var match = Regex.Match(content, @"<a([\S\s].*)?href=""[^""].*-id-(?<id>\d*)", RegexOptions.Multiline | RegexOptions.IgnoreCase);
      if (match.Success)
      {
        idType = "ID";
        return match.Groups["id"].Value;
      }

      match = Regex.Match(content, @"<a([\S\s].*)?href=""[^""].*-start-up-(?<id>\d*)", RegexOptions.Multiline | RegexOptions.IgnoreCase);
      if (match.Success)
      {
        idType = "STARTUP";
        return match.Groups["id"].Value;
      }

      idType = Constants.NotAvailable;
      return Constants.NotAvailable;
    }

    public string IdFromDetailLink(string content, out string idType)
    {
      var match = Regex.Match(content, @".*-id-(?<id>\d*)", RegexOptions.Multiline | RegexOptions.IgnoreCase);
      if (match.Success)
      {
        idType = "ID";
        return match.Groups["id"].Value;
      }

      match = Regex.Match(content, @".*-start-up-(?<id>\d*)", RegexOptions.Multiline | RegexOptions.IgnoreCase);
      if (match.Success)
      {
        idType = "STARTUP";
        return match.Groups["id"].Value;
      }

      idType = Constants.NotAvailable;
      return Constants.NotAvailable;
    }

    public string DetailLink(string content)
    {
      var match = Regex.Match(content, @"^<li.*(name=""(?<link>[^""].*))?""", RegexOptions.Multiline | RegexOptions.IgnoreCase);
      if (match.Success)
      {
        return match.Groups["link"].Value;
      }

      return Constants.NotAvailable;
    }

    public string DetailLinkListItem(string content)
    {
      var match = Regex.Match(content, @"<a.*href=""(?<href>[^""]*)""", RegexOptions.Multiline | RegexOptions.IgnoreCase);
      if (match.Success)
      {
        return match.Groups["href"].Value;
      }

      return Constants.NotAvailable;
    }

    public string RoomsFromLinkListItem(string content)
    {
      var match = Regex.Match(content, @"(?<rooms>\d+)-quarto(s)?", RegexOptions.Multiline | RegexOptions.IgnoreCase);
      if (match.Success)
      {
        return match.Groups["rooms"].Value;
      }

      return Constants.NotAvailable;
    }

    public string GaragesFromLinkListItem(string content)
    {
      var match = Regex.Match(content, @"com-garagem", RegexOptions.Multiline | RegexOptions.IgnoreCase);
      if (match.Success)
      {
        return "1";
      }

      return Constants.NotAvailable;
    }



    public string BusinessTypeFromLinkListItem(string content)
    {
      var match = Regex.Match(content, @"\-(?<businesstype>(venda|aluguel))\-", RegexOptions.Multiline | RegexOptions.IgnoreCase);
      if (match.Success)
      {
        return match.Groups["businesstype"].Value.ToUpper();
      }

      return Constants.NotAvailable;
    }


    public string BuildedAreaFromLinkListItem(string content)
    {
      var match = Regex.Match(content, @"\-(?<buildedarea>\d+)m2\-", RegexOptions.Multiline | RegexOptions.IgnoreCase);
      if (match.Success)
      {
        return match.Groups["buildedarea"].Value.ToUpper();
      }

      return Constants.NotAvailable;
    }


    public string DescriptionListItem(string content)
    {
      var match = Regex.Match(content, @"lu_description([^>])*>(?<description>[^<]*)<\/p>", RegexOptions.Multiline | RegexOptions.IgnoreCase);
      if (match.Success)
      {
        return match.Groups["description"].Value;
      }

      return Constants.NotAvailable;
    }


    public string CharacteristicAreaContent(string content)
    {
      var match = Regex.Match(content, @"lu_characteristics[^>]*>(?<content>.*)\<\/ul\>", RegexOptions.Singleline | RegexOptions.IgnoreCase);
      if (match.Success)
      {
        return match.Groups["content"].Value;
      }

      return Constants.NotAvailable;
    }


    public string SuitesFromCharacteristicListItem(string characteristicsContent)
    {
      var match = Regex.Match(characteristicsContent, @"Suites(.*lu_c_value)?[^>].*\>\s*(?<bathrooms>(\d+|[-]{2}))(.*\<)?", RegexOptions.Multiline | RegexOptions.IgnoreCase);
      if (match.Success)
      {
        return match.Groups["suites"].Value;
      }

      return Constants.NotAvailable;
    }


    public string GaragesFromCharacteristicListItem(string characteristicsContent)
    {
      var match = Regex.Match(characteristicsContent, @"Vagas(.*lu_c_value)?[^>].*\>\s*(?<bathrooms>(\d+|[-]{2}))(.*\<)?", RegexOptions.Multiline | RegexOptions.IgnoreCase);
      if (match.Success)
      {
        return match.Groups["garages"].Value;
      }

      return Constants.NotAvailable;
    }


    public string BathroomsFromCharacteristicListItem(string characteristicsContent)
    {
      var match = Regex.Match(characteristicsContent, @"Banheiros(.*lu_c_value)?[^>].*\>\s*(?<bathrooms>(\d+|[-]{2}))(.*\<)?", RegexOptions.Multiline | RegexOptions.IgnoreCase);
      if (match.Success)
      {
        return match.Groups["bathrooms"].Value;
      }

      return Constants.NotAvailable;
    }


    public string DetailInternalReference(WebBrowser browser)
    {
      var refContainer = browser.Document.GetElementsByTagName("ul").Cast<HtmlElement>().FirstOrDefault(item => item.GetAttribute("className").ToUpper() == "CODESINFO");
      if (refContainer != null)
      {
        return refContainer.GetElementsByTagName("li").Cast<HtmlElement>().ElementAt(0).GetElementsByTagName("strong").Cast<HtmlElement>().ElementAt(0).InnerText;
      }
      else
      {
        return Constants.NotAvailable;
      }
    }

    public HtmlElement DetailAttributesContainer(WebBrowser browser)
    {
      var refContainer = browser.Document.GetElementsByTagName("ul").Cast<HtmlElement>().FirstOrDefault(item => item.GetAttribute("className").ToUpper() == "ATTRIBUTELIST");
      return refContainer;

    }


    public string DetailAttributesArea(HtmlElement container, out string areaUnit)
    {
      var li = container.GetElementsByTagName("li").Cast<HtmlElement>().FirstOrDefault(item => item.GetAttribute("className").ToUpper().IndexOf("PROPERTYAREA") > -1);
      var mark = li.GetElementsByTagName("mark").Cast<HtmlElement>().ElementAt(0);
      string areaText;
      try
      {
        areaText = mark.GetElementsByTagName("span").Cast<HtmlElement>().ElementAt(0).OuterText;
      }
      catch (Exception ex)
      {
        areaText = mark.InnerText;
      }

      var match = Regex.Match(areaText, @"rea\s*:\s*(?<buildedarea>\d*)(\s+)?(?<areaType>[^\S].*)\s*$");
      if (match.Success)
      {
        if (match.Groups["areaType"].Value.Contains("m"))
        {
          areaUnit = "m2";
        }
        else
        {
          areaUnit = Constants.NotAvailable;
        }
        return match.Groups["buildedarea"].Value;
      }
      areaUnit = Constants.NotAvailable;
      return Constants.NotAvailable;
    }


    public string DetailAttributesRooms(HtmlElement container)
    {
      var li = container.GetElementsByTagName("li").Cast<HtmlElement>().FirstOrDefault(item => item.GetAttribute("className").ToUpper().IndexOf("PROPERTYROOMS") > -1);
      var mark = li.GetElementsByTagName("mark").Cast<HtmlElement>().ElementAt(0);
      string itemText;
      try
      {
        itemText = mark.GetElementsByTagName("span").Cast<HtmlElement>().ElementAt(0).OuterText;
      }
      catch (Exception ex)
      {
        itemText = mark.InnerText;
      }
      var match = Regex.Match(itemText, @"\s*(?<rooms>\d*)");
      if (match.Success)
      {
        return match.Groups["rooms"].Value;
      }

      return Constants.NotAvailable;
    }

    public Dictionary<string, string> DetailAttributes(HtmlElement container)
    {
      var lis = container.GetElementsByTagName("li").Cast<HtmlElement>();
      var result = new Dictionary<string, string>();
      foreach (var li in lis)
      {
        var classStr = li.GetAttribute("className").ToUpper();
        var title = classStr.Substring(classStr.IndexOf(' ')).Trim();

        var mark = li.GetElementsByTagName("mark").Cast<HtmlElement>().ElementAt(0);
        string dataText;
        try
        {
          dataText = mark.GetElementsByTagName("span").Cast<HtmlElement>().ElementAt(0).InnerHtml;
          var index = dataText.IndexOf('<') == -1 ? dataText.Length : dataText.IndexOf('<');
          dataText = dataText.Substring(0, index);
        }
        catch (Exception ex)
        {
          dataText = mark.InnerText;

        }

        var value = dataText.Trim();
        result.Add(title, value);
      }
      return result;
    }

    public string DetailAttributesBathrooms(HtmlElement container)
    {
      var li = container.GetElementsByTagName("li").Cast<HtmlElement>().FirstOrDefault(item => item.GetAttribute("className").ToUpper().IndexOf("PROPERTYBATHS") > -1);
      var mark = li.GetElementsByTagName("mark").Cast<HtmlElement>().ElementAt(0);
      string dataText;
      try
      {
        dataText = mark.GetElementsByTagName("span").Cast<HtmlElement>().ElementAt(0).OuterText;
      }
      catch (Exception ex)
      {
        dataText = mark.InnerText;

      }

      var match = Regex.Match(dataText, @"\s*(?<bathrooms>\d*)");
      if (match.Success)
      {
        return match.Groups["bathrooms"].Value;
      }

      return Constants.NotAvailable;
    }

    public string DetailAttributesGarages(HtmlElement container)
    {
      var li = container.GetElementsByTagName("li").Cast<HtmlElement>().FirstOrDefault(item => item.GetAttribute("className").ToUpper().IndexOf("PROPERTYGARAGES") > -1);
      if (li == null)
      {
        return Constants.NotAvailable;
      }

      var mark = li.GetElementsByTagName("mark").Cast<HtmlElement>().ElementAt(0);
      string dataText;
      try
      {
        dataText = mark.GetElementsByTagName("span").Cast<HtmlElement>().ElementAt(0).InnerText;
      }
      catch (Exception ex)
      {
        dataText = mark.InnerText;
      }

      var match = Regex.Match(dataText, @"\s*(?<garages>\d*)");
      if (match.Success)
      {
        return match.Groups["garages"].Value;
      }

      return Constants.NotAvailable;
    }

    public Dictionary<string, string> DetailCharacteristics(WebBrowser browser)
    {
      var temp = browser.Document.GetElementsByTagName("div").Cast<HtmlElement>().FirstOrDefault(item => item.GetAttribute("className").ToUpper().IndexOf("PROPERTYFEATURES") > -1);
      var result = new Dictionary<string, string>();
      if (temp != null)
      {
        var charContainer = temp.GetElementsByTagName("ul").Cast<HtmlElement>().ElementAt(0);
        var lis = charContainer.GetElementsByTagName("li").Cast<HtmlElement>();

        foreach (var li in lis)
        {
          var title = li.InnerText.ToUpper();
          var value = "1";
          result.Add(title, value);
        }
      }

      return result;
    }

    public string DetailCharacteristicConditionerAir(WebBrowser browser)
    {
      var temp = browser.Document.GetElementsByTagName("div").Cast<HtmlElement>().FirstOrDefault(item => item.GetAttribute("className").ToUpper().IndexOf("PROPERTYFEATURES") > -1);
      if (temp != null)
      {
        var charContainer = temp.GetElementsByTagName("ul").Cast<HtmlElement>().ElementAt(0);
        var hasCharact = charContainer.GetElementsByTagName("li").Cast<HtmlElement>().FirstOrDefault(item => item.InnerText.ToUpper().Contains("AR CONDICIONADO")) != null;

        return hasCharact ? "1" : Constants.NotAvailable;
      }

      return Constants.NotAvailable;
    }

    public string DetailCharacteristicPool(WebBrowser browser)
    {
      var temp = browser.Document.GetElementsByTagName("div").Cast<HtmlElement>().FirstOrDefault(item => item.GetAttribute("className").ToUpper().IndexOf("PROPERTYFEATURES") > -1);
      if (temp != null)
      {
        var charContainer = temp.GetElementsByTagName("ul").Cast<HtmlElement>().ElementAt(0);
        var hasCharact = charContainer.GetElementsByTagName("li").Cast<HtmlElement>().FirstOrDefault(item => item.InnerText.ToUpper().Contains("PISCINA")) != null;

        return hasCharact ? "1" : Constants.NotAvailable;
      }
      return Constants.NotAvailable;

    }


    public string DetailCharacteristicInterphone(WebBrowser browser)
    {
      var temp = browser.Document.GetElementsByTagName("div").Cast<HtmlElement>().FirstOrDefault(item => item.GetAttribute("className").ToUpper().IndexOf("PROPERTYFEATURES") > -1);
      if (temp != null)
      {
        var charContainer = temp.GetElementsByTagName("ul").Cast<HtmlElement>().ElementAt(0);
        var hasCharact = charContainer.GetElementsByTagName("li").Cast<HtmlElement>().FirstOrDefault(item => item.InnerText.ToUpper().Contains("INTERFONE")) != null;

        return hasCharact ? "1" : Constants.NotAvailable;
      }

      return Constants.NotAvailable;
    }



    public string DetailDescription(WebBrowser browser, out string title)
    {
      var article = browser.Document.GetElementsByTagName("article").Cast<HtmlElement>().FirstOrDefault(item => item.GetAttribute("className").ToUpper() == "PROPERTYDESCRIPTION");
      if (article != null)
      {
        title = article.GetElementsByTagName("h2").Cast<HtmlElement>().ElementAt(0).InnerText;
        var description = article.GetElementsByTagName("p").Cast<HtmlElement>();
        if (description != null && description.Count() > 0)
        {
          return description.ElementAt(0).InnerText;
        }
      }

      title = null;
      return Constants.NotAvailable;
    }


    public List<POC_Core.Models.Media> DetailThumbnails(WebBrowser browser)
    {
      var thumbsContainer = browser.Document.GetElementsByTagName("div").Cast<HtmlElement>().FirstOrDefault(item => item.GetAttribute("className").ToUpper() == "THUMBSWRAPPER");
      var thumbItems = thumbsContainer.GetElementsByTagName("ul").Cast<HtmlElement>().ElementAt(0).GetElementsByTagName("li").Cast<HtmlElement>().ToList();

      var caurrouselContainer = browser.Document.GetElementsByTagName("div").Cast<HtmlElement>().FirstOrDefault(item => item.GetAttribute("className").ToUpper() == "MAINCARROUSEL");
      var medias = caurrouselContainer.GetElementsByTagName("ul").Cast<HtmlElement>().ElementAt(0).GetElementsByTagName("li").Cast<HtmlElement>().ToList();


      var thumbs = new List<POC_Core.Models.Media>();

      string thumbnailUrl = null;
      string frameborder = null, framewidth = null;
      bool? frameAllowFullscreen = null;
      string mediaUrl = null, mediaTitle = null;
      var mediaType = MediaType.Picture;

      var counter = 0;
      thumbItems.ForEach(item =>
      {
        var aTumbs = item.GetElementsByTagName("a").Cast<HtmlElement>().ElementAt(0);
        var thumbnailTitle = aTumbs.GetAttribute("title");
        var thumbMedia = aTumbs.Children.Cast<HtmlElement>().ElementAt(0);

        if (thumbMedia.TagName.ToUpper() == "IMG")
        {
          thumbnailUrl = thumbMedia.GetAttribute("src");

          var mediaElement = medias.ElementAt(counter);

          var mediaWrapper = mediaElement.Children.Cast<HtmlElement>().ElementAt(0);
          if (mediaWrapper.TagName.ToUpper() == "IMG")
          {
            mediaTitle = mediaElement.GetAttribute("title");
            mediaUrl = mediaElement.GetElementsByTagName("img").Cast<HtmlElement>().ElementAt(0).GetAttribute("src");
          }
          else if (thumbMedia.TagName.ToUpper() == "IFRAME")
          {

            mediaUrl = thumbMedia.GetAttribute("src");
            frameborder = thumbMedia.GetAttribute("frameBorder");
            framewidth = thumbMedia.GetAttribute("width");
            frameAllowFullscreen = thumbMedia.GetAttribute("allowfullscreen") != null;
            mediaType = MediaType.Video;
          }
        }



        counter++;

        thumbs.Add(new POC_Core.Models.Media
        {
          ThumbnailTitle = thumbnailTitle,
          ThumbnailUrl = thumbnailUrl,
          MediaTitle = mediaTitle,
          MediaUrl = mediaUrl,
          MediaType = mediaType,
          Frameborder = frameborder,
          Framewidth = framewidth,
          FrameAllowFullscreen = frameAllowFullscreen
        });
      });


      return thumbs;

    }
    #endregion

    public List<CharacteristicValue> ConvertToCharacteristics(Dictionary<string, string> rawAttributes)
    {

      var result = new List<CharacteristicValue>();

      var propertyContext = new PropertyContextClass();
      var propertyRepository = new PropertyRepository(propertyContext);
      int characteristicId = -1;
      CharacteristicValue characteristicValue = null;
      foreach (var keyvalue in rawAttributes)
      {
        characteristicValue = null;
        var name = keyvalue.Key;
        var value = keyvalue.Value;
        try
        {
          characteristicId = this.RetrieveCharacteristic(name);
        }
        catch (ArgumentOutOfRangeException ex)
        {
          characteristicValue = new CharacteristicValue { Characteristic = new CharacteristicNotMapped { Identifier = name, CategoryId = (int)CategoriesEnum.UNDEFINED} };
        }
        catch (ArgumentException ex)
        {
          continue;
        }

        if (characteristicValue == null)
        {
          characteristicValue = new CharacteristicValue { CharacteristicId = characteristicId };
        }
        characteristicValue.Value = value;
        characteristicValue.EntityState = CustomEntityState.Added;

        result.Add(characteristicValue);
      }

      return result;
    }

    private int RetrieveCharacteristic(string name)
    {
      var characteristicId = -1;
      switch (name.ToUpper())
      {
        case "ROOMS":
          characteristicId = 1;
          break;

        case "SUITES":
          characteristicId = 2;
          break;
        case "BATHROOMS":
          characteristicId = 2;
          break;


        case "PRICE":
          characteristicId = 2;
          break;

        case "AREA"://Builded area
          characteristicId = 2;
          break;
        case "AREAUNIT":
          characteristicId = 2;
          break;
        case "GARAGES":
          characteristicId = 2;
          break;
        case "LATITUDE":
          characteristicId = 2;
          break;
        case "LONGITUDE":
          characteristicId = 2;
          break;
        case "ISDEVELOPMENTUNIT":
          characteristicId = 2;
          break;
        case "SAVED":
          characteristicId = 2;
          break;
        case "LISTINGTYPE":
          characteristicId = 2;
          break;
        case "EXTERNALID":
          characteristicId = 2;
          break;
        case "PROPERTYTYPENAME":
          characteristicId = 2;
          break;

        case "TITLE":
          characteristicId = 2;
          break;
        case "LEGEND":
          characteristicId = 2;
          break;
        case "ACCOUNTNAME":
          characteristicId = 2;
          break;
        case "ACCOUNTLOGO":
          characteristicId = 2;
          break;
        case "ACCOUNTROLE":
          characteristicId = 2;
          break;
        case "ACCOUNTLICENSENUMBER":
          characteristicId = 2;
          break;
        case "ACCOUNT":
          characteristicId = 2;
          break;
        case "EMAIL":
          characteristicId = 2;
          break;
        case "LEADEMAILS":
          characteristicId = 2;
          break;
        case "CONTACTNAME":
          characteristicId = 2;
          break;
        case "CONTACTLOGO":
          characteristicId = 2;
          break;
        case "CONTACTPHONENUMBER":
          characteristicId = 2;
          break;
        case "CONTACTCELLPHONENUMBER":
          characteristicId = 2;
          break;
        case "CONTACTADDRESS":
          characteristicId = 2;
          break;
        case "USAGEID":
          characteristicId = 2;
          break;
        case "BUSINESSID":
          characteristicId = 2;
          break;
        case "PUBLICATIONTYPE":
          characteristicId = 2;
          break;
        case "SALEPRICE":
          characteristicId = 2;
          break;
        case "BASESALEPRICE":
          characteristicId = 2;
          break;
        case "RENTPRICE":
          characteristicId = 2;
          break;
        case "BASERENTPRICE":
          characteristicId = 2;
          break;
        case "CURRENCY":
          characteristicId = 2;
          break;
        case "SHOWADDRESS":
          characteristicId = 2;
          break;
        case "ADDRESS":
          characteristicId = 2;
          break;
        case "ZIPCODE":
          characteristicId = 2;
          break;
        case "VIDEO":
          characteristicId = 2;
          break;
        case "CONSTRUCTIONSTATUS":
          characteristicId = 2;
          break;
        case "RENTPERIODID":
          characteristicId = 2;
          break;
        case "CONDOMINIUMPRICE":
          characteristicId = 2;
          break;
        case "IPTU":
          characteristicId = 2;
          break;
        case "DEVELOPMENTINFORMATION":
          characteristicId = 2;
          break;

        case "PROPERTYTYPEID":
        case "USAGENAME":
        case "BUSINESSNAME":
        case "RENTPERIOD":
          throw new ArgumentException("Characteristic not allowed!");

        default:
          throw new ArgumentOutOfRangeException("Characteristic mapping not found!");
      }

      return characteristicId;
    }

  }
}
