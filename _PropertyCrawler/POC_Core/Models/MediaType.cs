﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POC_Core.Models
{
  public enum MediaType
  {
    Picture = 1,
    Video = 2
  }
}
