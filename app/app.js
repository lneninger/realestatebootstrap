define(['angularAMD', 'angular-route', 'bootstrap/ui-bootstrap-tpls.min', 'bootstrap/bootstrap.min', 'app-commons/customSliderDirective', 'app-commons/footerDirective', 'app-commons/hmenuDirective', 'app-commons/loginDirective', 'app-commons/propertyInlineCaroselDirective', 'app-commons/propertyFilterDirective', 'app-commons/toasterDirective', 'common-tools'], function(angularAMD) {
  var myApp = new angular.module('myApp', ['ngRoute', 'ui.bootstrap', 'toaster']);
  
  myApp.config(function($routeProvider) {
    $routeProvider.when('/', angularAMD.route({
      templateUrl: 'templates/home.html',
      controller: 'mainController',
      controllerUrl: '.../../../app/controllers/mainController'
    }));

    $routeProvider.when('/search', angularAMD.route({
      templateUrl: 'templates/search.html',
      controller: 'searchController',
      controllerUrl: '.../../../app/controllers/searchController'
    }));
    
    $routeProvider.when('/contact', angularAMD.route({
      templateUrl: 'templates/contact.html',
      controller: 'contactController',
      controllerUrl: '.../../../app/controllers/contactController'
    }));

    $routeProvider.when('/aboutus', angularAMD.route({
      templateUrl: 'templates/aboutus.html',
      controller: 'aboutusController',
      controllerUrl: '.../../../app/controllers/aboutusController'
    }));


    $routeProvider.otherwise({
      redirectTo: '/'
    });
  });

  return angularAMD.bootstrap(myApp);

});
