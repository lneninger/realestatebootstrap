﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EntityFrameworkModeling;
using ModelEntities;
using PropertyContext;

namespace Properties.Web.API.Controllers
{
  public class CharacteristicsController : ApiController
  {
    private PropertyContextClass db = new PropertyContextClass();

    // GET: api/Characteristics
    public IQueryable<Characteristic> GetCharacteristics()
    {
      return db.Characteristics;
    }

    // GET: api/Characteristics/5
    [ResponseType(typeof(Characteristic))]
    public async Task<IHttpActionResult> GetCharacteristic(int id)
    {
      Characteristic characteristic = await db.Characteristics.FindAsync(id);
      if (characteristic == null)
      {
        return NotFound();
      }

      return Ok(characteristic);
    }

    // PUT: api/Characteristics/5
    [ResponseType(typeof(void))]
    public async Task<IHttpActionResult> PutCharacteristic(int id, Characteristic characteristic)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      if (id != characteristic.Id)
      {
        return BadRequest();
      }

      db.Entry(characteristic).State = EntityState.Modified;

      try
      {
        await db.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        if (!CharacteristicExists(id))
        {
          return NotFound();
        }
        else
        {
          throw;
        }
      }

      return StatusCode(HttpStatusCode.NoContent);
    }

    // POST: api/Characteristics
    [ResponseType(typeof(Characteristic))]
    public async Task<IHttpActionResult> PostCharacteristic(Characteristic characteristic)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      db.Characteristics.Add(characteristic);
      await db.SaveChangesAsync();

      return CreatedAtRoute("DefaultApi", new { id = characteristic.Id }, characteristic);
    }

    // DELETE: api/Characteristics/5
    [ResponseType(typeof(Characteristic))]
    public async Task<IHttpActionResult> DeleteCharacteristic(int id)
    {
      Characteristic characteristic = await db.Characteristics.FindAsync(id);
      if (characteristic == null)
      {
        return NotFound();
      }

      db.Characteristics.Remove(characteristic);
      await db.SaveChangesAsync();

      return Ok(characteristic);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        db.Dispose();
      }
      base.Dispose(disposing);
    }

    private bool CharacteristicExists(int id)
    {
      return db.Characteristics.Count(e => e.Id == id) > 0;
    }
  }
}