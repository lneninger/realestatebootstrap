define(['angularAMD'], function(angularAMD) {
  angularAMD.directive('footerSession', function($location) {
    return {
      restrict: "E",
      templateUrl: '.../../templates/directives/footer.html'
    };
  });

});
