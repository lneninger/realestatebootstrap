﻿using EntityFrameworkModeling;
using ModelEntities;
using ModelEntities.Location;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyContext
{
  public class PropertyContextClass : BaseContext<PropertyContextClass>
  {
    public const string SchemaName = "Properties";
    public DbSet<Property> Properties { get; set; }


    /*Locations*/

    public DbSet<ZipCode> ZipCodes { get; set; }
    public DbSet<Neighborhood> Neighborhoods { get; set; }
    public DbSet<City> Cities { get; set; }
    public DbSet<State> States { get; set; }
    public DbSet<Country> Countries { get; set; }

    /*Domains*/
    public DbSet<ModelEntities.Type> Types { get; set; }
    public DbSet<Characteristic> Characteristics { get; set; }
    public DbSet<Category> Categories { get; set;}
    public DbSet<TypeCharacteristic> TypeCharacteristics { get; set; }
    

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
      this.MapPropertyEntity(modelBuilder);

      this.MapMediaEntity(modelBuilder);

      this.MapMediaPendingDownloadEntity(modelBuilder);

      this.MapCharacteristicValueEntity(modelBuilder);


      base.OnModelCreating(modelBuilder);
    }

    private void MapCharacteristicValueEntity(DbModelBuilder modelBuilder)
    {
      /*CharacteristicValues*/
      modelBuilder.Entity<CharacteristicValue>()
        .ToTable("CharacteristicValues", SchemaName)
        .HasKey(t => t.Id);

      modelBuilder.Entity<CharacteristicValue>()
        .Property(t => t.Id)
        .HasColumnName("CD_CharacteristicValue")
        .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

      modelBuilder.Entity<CharacteristicValue>()
      .Ignore(t => t.EntityState);

      modelBuilder.Entity<CharacteristicValue>()
        .Property(t => t.Value)
        .HasColumnName("DS_Value");


      modelBuilder.Entity<CharacteristicValue>()
        .HasRequired(t => t.Property);


      modelBuilder.Entity<CharacteristicValue>()
        .Property(t => t.PropertyId)
        .HasColumnName("CD_Property");


      modelBuilder.Entity<CharacteristicValue>()
        .HasRequired(t => t.Characteristic);

      modelBuilder.Entity<CharacteristicValue>()
        .Property(t => t.CharacteristicId)
        .HasColumnName("CD_Characteristic1");

    }

    private void MapMediaPendingDownloadEntity(DbModelBuilder modelBuilder)
    {
      /*MediaPendingDownload*/
      modelBuilder.Entity<MediaPendingDownload>()
       .ToTable("PropertyMediaDownload", "Pendings")
       .HasKey<Guid>(t => t.Id);

      modelBuilder.Entity<MediaPendingDownload>()
      .Property(t => t.Id)
        .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
        .HasColumnName("CD_Media");


      modelBuilder.Entity<MediaPendingDownload>()
      .Ignore(t => t.EntityState);

      modelBuilder.Entity<MediaPendingDownload>()
      .Property<long>(t => t.PropertyId)
        .HasColumnName("CD_Property")
        .IsRequired();

      modelBuilder.Entity<MediaPendingDownload>()
       .Property(t => t.ThumbnailUrl)
         .HasColumnName("DS_ThumbnailUrl")
         .HasMaxLength(2000)
         .IsOptional();

      modelBuilder.Entity<MediaPendingDownload>()
       .Property(t => t.MediaUrl)
         .HasColumnName("DS_MediaUrl")
         .HasMaxLength(2000)
         .IsOptional();
    }

    private void MapMediaEntity(DbModelBuilder modelBuilder)
    {
      /*Medias*/
      modelBuilder.Entity<Media>()
       .ToTable("PropertyMedias", SchemaName)
       .HasKey<Guid>(t => t.Id);

      modelBuilder.Entity<Media>()
      .Property(t => t.Id)
        .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
        .HasColumnName("CD_Media");


      modelBuilder.Entity<Media>()
      .Ignore(t => t.EntityState);

      modelBuilder.Entity<Media>()
      .Property<long>(t => t.PropertyId)
        .HasColumnName("CD_Property")
        .IsRequired();

      modelBuilder.Entity<Media>()
       .Property(t => t.ThumbnailTitle)
         .HasColumnName("NM_ThumbnailTitle")
         .HasMaxLength(200)
         .IsOptional();

      modelBuilder.Entity<Media>()
       .Property(t => t.ThumbnailUrl)
         .HasColumnName("DS_ThumbnailUrl")
         .HasMaxLength(2000)
         .IsOptional();

      modelBuilder.Entity<Media>()
       .Property(t => t.MediaTitle)
         .HasColumnName("NM_MediaTitle")
         .HasMaxLength(200)
         .IsOptional();

      modelBuilder.Entity<Media>()
       .Property(t => t.MediaUrl)
         .HasColumnName("DS_MediaUrl")
         .HasMaxLength(2000)
         .IsOptional();

      modelBuilder.Entity<Media>()
      .Property(t => t.Active)
        .HasColumnName("IN_Active");


      /*Types*/
      modelBuilder.Entity<ModelEntities.Type>()
      .ToTable("Types", SchemaName)
      .HasKey<long>(t => t.Id);

      modelBuilder.Entity<ModelEntities.Type>()
      .Property(t => t.Id)
      .HasColumnName("CD_Type");

      modelBuilder.Entity<ModelEntities.Type>()
     .Property(t => t.Identifier)
     .HasColumnName("NM_Identifier")
     .HasMaxLength(2000);

      modelBuilder.Entity<ModelEntities.Type>()
      .HasMany(t => t.TypeCharacteristics)
      .WithRequired(t => t.Type)
      .HasForeignKey(t => t.TypeId);

      /*Characteristics*/
      modelBuilder.Entity<Characteristic>()
    .Map<Characteristic>(m => m.Requires("IN_Active").HasValue(true))
    .Map<CharacteristicNotMapped>(m => m.Requires("IN_Active").HasValue(false))
      .ToTable("Characteristics", SchemaName)
      .HasKey<long>(t => t.Id);

      modelBuilder.Entity<Characteristic>()
    .Property(t => t.Id)
    .HasColumnName("CD_Characteristic");

      modelBuilder.Entity<Characteristic>()
      .Property(t => t.Identifier)
      .HasColumnName("NM_Identifier")
      .HasMaxLength(64);

      modelBuilder.Entity<Characteristic>()
     .Property(t => t.DataType)
     .HasColumnName("NM_DataType")
     .HasMaxLength(64);

      modelBuilder.Entity<Characteristic>()
     .Property(t => t.CategoryId)
     .HasColumnName("CD_Category");

      modelBuilder.Entity<Characteristic>()
       .HasMany(t => t.TypeCharacteristics)
       .WithRequired(t => t.Characteristic)
       .HasForeignKey(t => t.CharacteristicId);


      modelBuilder.Entity<CharacteristicNotMapped>()
        .Property(t => t.PropertySource)
        .HasColumnName("CD_PropertySource");


      modelBuilder.Entity<CharacteristicNotMapped>()
      .Ignore(t => t.EntityState);



      /*Category*/
      modelBuilder.Entity<Category>()
      .ToTable("Categories", SchemaName)

      .HasKey<int>(t => t.Id);

      modelBuilder.Entity<Category>()
      .Property(t => t.Id)
      .HasColumnName("CD_Category");

      modelBuilder.Entity<Category>()
      .Property(t => t.Identifier)
      .HasColumnName("NM_Identifier")
      .HasMaxLength(64);

      modelBuilder.Entity<Category>()
       .HasMany(t => t.Characteristics)
       .WithRequired(t => t.Category)
       .HasForeignKey(t => t.CategoryId);

      /*TypeCharacteristic*/
      modelBuilder.Entity<TypeCharacteristic>()
      .ToTable("TypeCharacteristics", SchemaName)
      .HasKey<int>(t => t.Id);

      modelBuilder.Entity<TypeCharacteristic>()
       .Property<int>(t => t.Id)
       .HasColumnName("CD_TypeCharacteristic");

      modelBuilder.Entity<TypeCharacteristic>()
       .Property<int>(t => t.Order)
       .HasColumnName("NR_Order");

      modelBuilder.Entity<TypeCharacteristic>()
       .HasRequired(t => t.Characteristic);

      modelBuilder.Entity<TypeCharacteristic>()
       .Property(t => t.CharacteristicId)
       .HasColumnName("CD_Characteristic");

      modelBuilder.Entity<TypeCharacteristic>()
      .Property(t => t.TypeId)
      .HasColumnName("CD_Type");

      modelBuilder.Entity<TypeCharacteristic>()
       .HasRequired(t => t.Type)
       .WithMany(t => t.TypeCharacteristics)
       .HasForeignKey(t => t.TypeId);


      /*ZipCodes*/
      modelBuilder.Entity<ZipCode>()
      .ToTable("ZipCodes", SchemaName)
      .HasKey<Guid>(t => t.Id);

    }

    private void MapPropertyEntity(DbModelBuilder modelBuilder)
    {
      /*Properties*/
      modelBuilder.Entity<Property>()
      .ToTable("Properties", SchemaName)
      .HasKey<long>(t => t.Id);

      modelBuilder.Entity<Property>()
      .Property(t => t.Id)
      .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
      .HasColumnName("CD_Property");

      modelBuilder.Entity<Property>()
      .Ignore(t => t.EntityState);

      modelBuilder.Entity<Property>()
     .Property(t => t.PropertySource)
     .HasColumnName("CD_PropertySource");

      modelBuilder.Entity<Property>()
     .Property(t => t.ExternalId)
     .HasColumnName("NM_ExternalId")
     .HasMaxLength(2000);

      modelBuilder.Entity<Property>()
      .HasMany(t => t.CharacteristicValues)
      .WithRequired(t => t.Property)
      .HasForeignKey(t => t.PropertyId)
      .WillCascadeOnDelete(true);
    }

  }
}
