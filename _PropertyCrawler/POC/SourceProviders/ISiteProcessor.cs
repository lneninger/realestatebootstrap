﻿using POC_Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace POC.SourceProviders
{
    public interface ISiteProcessor
    {
        void Process();

        WebBrowser WebBrowser { get; set; }

        IInfoParser InfoParser { get; set; }

        List<Property> Properties { get; }

        List<string> UrlHistory { get; set; }
    }
}
