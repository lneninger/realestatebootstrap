

function domBinder(){
	$('[data-toggle="tooltip"]').tooltip();
}

function domTooltipBinder(selector){
	if(!selector)
		selector = '';

	if(typeof selector === 'string')
		selector = selector + '[data-toggle="tooltip"],[data-tooltip]';
	
	$(selector).tooltip();
};


function setValidatorDefaults() {
	$.validator.setDefaults({
	    highlight: function(element) {
	        $(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
	    errorElement: 'span',
	    errorClass: 'help-block',
	    errorPlacement: function(error, element) {
	        if(element.parent('.input-group').length) {
	            error.insertAfter(element.parent());
	        } else {
	            error.insertAfter(element);
	        }
	    }
	});
};


$(document).ready(function(){
	setValidatorDefaults();
});


