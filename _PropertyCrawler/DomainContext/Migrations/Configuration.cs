namespace DomainContext.Migrations
{
  using ModelEntities;
  using System;
  using System.Collections.Generic;
  using System.Data.Entity;
  using System.Data.Entity.Migrations;
  using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DomainContext.DomainContextClass>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DomainContext.DomainContextClass context)
        {
         
        }
    }
}
