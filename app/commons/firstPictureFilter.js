define(['app'], function(myApp){
	myApp.filter('firstPicture', function() {
	  	return function(medias) {
		    for(var i = 0; i < medias.length; i++)
		    {
		    	var media = medias[i];
		    	if(media)
		    	{
		    		return media.ds_thumbnail;
		    	}
		    }
	  	};
	});
});