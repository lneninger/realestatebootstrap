﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModelEntities
{
  public class TypeCharacteristic
  {
    public int Id { get; set; }

    public int Order { get; set; }

    public int CharacteristicId { get; set; }

    public int TypeId { get; set; }

    public ModelEntities.Characteristic Characteristic { get; set; }

    public ModelEntities.Type Type { get; set; }
  }
}
