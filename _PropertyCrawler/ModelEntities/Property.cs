﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelEntities
{
  public class Property : IObjectWithState
  {
    public long Id { get; set; }

    public ICollection<Media> Medias { get; set; }

    public ICollection<CharacteristicValue> CharacteristicValues { get; set; }

    public PropertySourcesEnum PropertySource { get; set; }
    public string ExternalId { get; set; }
    public CustomEntityState EntityState { get; set; }
  }
}
