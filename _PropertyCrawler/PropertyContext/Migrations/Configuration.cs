namespace PropertyContext.Migrations
{
  using ModelEntities;
  using System;
  using System.Collections.Generic;
  using System.Data.Entity;
  using System.Data.Entity.Migrations;
  using System.Linq;

  internal sealed class Configuration : DbMigrationsConfiguration<PropertyContext.PropertyContextClass>
  {
    public Configuration()
    {
      AutomaticMigrationsEnabled = false;
    }

    protected override void Seed(PropertyContext.PropertyContextClass context)
    {
      context.Categories.AddOrUpdate(

new Category
{
  Id = 2,
  Identifier = "IDS",
  Characteristics = new List<Characteristic>() { 
              new Characteristic{CategoryId = 2, Id = 1, DataType = "STTRING", Identifier = "ID_EXTERNAL1"},
              new Characteristic{CategoryId = 2, Id = 2, DataType = "STRING", Identifier = "ID_EXTERNAL2"}
              }
}, new Category
{
  Id = 3,
  Identifier = "PRICE",
  Characteristics = new List<Characteristic>() { 
              new Characteristic{CategoryId = 3, Id = 3, DataType = "DECIMAL", Identifier = "PRICE_SELL"},
              new Characteristic{CategoryId = 3, Id = 4, DataType = "STRING", Identifier = "SELL_CURRENCY"},
              new Characteristic{CategoryId = 3, Id = 5, DataType = "DECIMAL", Identifier = "PRICE_RENT"},
              new Characteristic{CategoryId = 3, Id = 6, DataType = "STRING", Identifier = "RENT_CURRENCY"},
              new Characteristic{CategoryId = 3, Id = 7, DataType = "STRING", Identifier = "PRICE_CONDOMINIUM"},
              new Characteristic{CategoryId = 3, Id = 8, DataType = "STRING", Identifier = "TAX_PROPERTY"}
              }
},
new Category
{
  Id = 4,
  Identifier = "AREA",
  Characteristics = new List<Characteristic>() { 
              new Characteristic{CategoryId = 4, Id = 9, DataType = "DECIMAL", Identifier = "AREA_BUILDED"},
              new Characteristic{CategoryId = 4, Id = 10, DataType = "DECIMAL", Identifier = "AREA_TOTAL"},
              new Characteristic{CategoryId = 4, Id = 11, DataType = "DECIMAL", Identifier = "AREA_COMMON"}
              }
},
new Category
{
  Id = 5,
  Identifier = "LEISURE",
  Characteristics = new List<Characteristic>() { 
              new Characteristic{CategoryId = 5, Id = 12, DataType = "INTEGER", Identifier = "POOL"},
              new Characteristic{CategoryId = 5, Id = 13, DataType = "INTEGER", Identifier = "SPA"},
              new Characteristic{CategoryId = 5, Id = 14, DataType = "INTEGER", Identifier = "OFURO"},
              new Characteristic{CategoryId = 5, Id = 15, DataType = "INTEGER", Identifier = "SAUNA"},
              new Characteristic{CategoryId = 5, Id = 16, DataType = "INTEGER", Identifier = "TENNIS_COURT"},
              new Characteristic{CategoryId = 5, Id = 17, DataType = "INTEGER", Identifier = "PARTY_SALOUM"},
              new Characteristic{CategoryId = 5, Id = 18, DataType = "INTEGER", Identifier = "PLAYGROUND"},
              }
},
new Category
{
  Id = 6,
  Identifier = "OUTDOOR",
  Characteristics = new List<Characteristic>() { 
              new Characteristic{CategoryId = 6, Id = 19, DataType = "INTEGER", Identifier = "YARD"},
              new Characteristic{CategoryId = 6, Id = 20, DataType = "INTEGER", Identifier = "BACKYARD"},
              new Characteristic{CategoryId = 6, Id = 21, DataType = "INTEGER", Identifier = "BALCONY"},
              new Characteristic{CategoryId = 6, Id = 22, DataType = "INTEGER", Identifier = "RIVER"},
              new Characteristic{CategoryId = 6, Id = 23, DataType = "INTEGER", Identifier = "LAKE"},
              new Characteristic{CategoryId = 6, Id = 24, DataType = "INTEGER", Identifier = "FOOTINSAND"},
             
              }
},
new Category
{
  Id = 7,
  Identifier = "STRUCTURE",
  Characteristics = new List<Characteristic>() { 
              new Characteristic{CategoryId = 7, Id = 25, DataType = "INTEGER", Identifier = "ROOMS"},
              new Characteristic{CategoryId = 7, Id = 26, DataType = "INTEGER", Identifier = "SUITES"},
              new Characteristic{CategoryId = 7, Id = 27, DataType = "DECIMAL", Identifier = "BATHROOMS"},
              new Characteristic{CategoryId = 7, Id = 28, DataType = "INTEGER", Identifier = "LIVING_ROOMS"},
              new Characteristic{CategoryId = 7, Id = 29, DataType = "INTEGER", Identifier = "KITCHENS"},
              new Characteristic{CategoryId = 7, Id = 30, DataType = "INTEGER", Identifier = "SERVICE_AREA"},
              }
},
new Category
{
  Id = 8,
  Identifier = "FURNITURE",
  Characteristics = new List<Characteristic>() { 
              new Characteristic{CategoryId = 8, Id = 31, DataType = "INTEGER", Identifier = "ROOM_CLOSETS"},
              new Characteristic{CategoryId = 8, Id = 32, DataType = "INTEGER", Identifier = "SUITE_CLOSETS"},
              new Characteristic{CategoryId = 8, Id = 33, DataType = "INTEGER", Identifier = "BATHROOM_CLOSETS"},
              new Characteristic{CategoryId = 8, Id = 34, DataType = "INTEGER", Identifier = "KITCHEN_CLOSETS"}
              }
},
new Category
{
  Id = 9,
  Identifier = "LOCATION",
  Characteristics = new List<Characteristic>() { 
              new Characteristic{CategoryId = 9, Id = 35, DataType = "STRING", Identifier = "LATITUDE"},
              new Characteristic{CategoryId = 9, Id = 36, DataType = "STRING", Identifier = "LONGITUDE"},
              new Characteristic{CategoryId = 9, Id = 37, DataType = "STRING", Identifier = "ZIP_CODE"},
              new Characteristic{CategoryId = 9, Id = 38, DataType = "STRING", Identifier = "COUNTRY"},
              new Characteristic{CategoryId = 9, Id = 39, DataType = "STRING", Identifier = "STATE"},
              new Characteristic{CategoryId = 9, Id = 40, DataType = "STRING", Identifier = "REGION"},
              new Characteristic{CategoryId = 9, Id = 41, DataType = "STRING", Identifier = "CITY"},
              new Characteristic{CategoryId = 9, Id = 42, DataType = "STRING", Identifier = "NEIGHBORHOOD"},
              new Characteristic{CategoryId = 9, Id = 43, DataType = "STRING", Identifier = "ADDRESS"},
              new Characteristic{CategoryId = 9, Id = 44, DataType = "STRING", Identifier = "ADDRESS_NUMBER"}
              }
},
new Category
{
  Id = 1,
  Identifier = "DESCRIPTION",
  Characteristics = new List<Characteristic>() { 
              new Characteristic{CategoryId = 1, Id = 45, DataType = "STRING", Identifier = "TITLE"},
              new Characteristic{CategoryId = 1, Id = 46, DataType = "STRING", Identifier = "DESCRIPTION"}
              }
}
);

      context.Types.AddOrUpdate(
        new ModelEntities.Type
        {
          Id = 1,
          Identifier = "HOUSE",
        },
        new ModelEntities.Type
        {
          Id = 2,
          Identifier = "APARTMENT",
        },
        new ModelEntities.Type
        {
          Id = 3,
          Identifier = "FLAT",
        },
        new ModelEntities.Type
        {
          Id = 4,
          Identifier = "SHOP",
        },
        new ModelEntities.Type
        {
          Id = 5,
          Identifier = "OUTLET",
        },
        new ModelEntities.Type
        {
          Id = 6,
          Identifier = "OFFICE",
        },
        new ModelEntities.Type
        {
          Id = 7,
          Identifier = "SHED",
        },
        new ModelEntities.Type
        {
          Id = 8,
          Identifier = "LAND",
        },
        new ModelEntities.Type
        {
          Id = 9,
          Identifier = "FARM",
        },
        new ModelEntities.Type
        {
          Id = 10,
          Identifier = "HOME_SITE",
        });


      var houseCharacteristics = from type in context.Types
                                 from characteristic in context.Characteristics
                                 where type.Identifier == "HOUSE"
                                 && new string[] {
                                  "POOL", "SPA", "OFURO", "SAUNA", "TENNIS_COURT", "PARTY_SALOUM", "PLAYGROUND", 
                                  "YARD", "BACKYARD", "BALCONY", "RIVER", "LAKE", "FOOTINSAND", "ROOMS", "SUITES", "BATHROOMS", 
                                  "LIVING_ROOMS", "KITCHENS", "SERVICE_AREA", "ROOM_CLOSETS", "SUITE_CLOSETS", 
                                  "BATHROOM_CLOSETS", "KITCHEN_CLOSETS"
                                 }.Contains(characteristic.Identifier)
                                 select new { TypeId = type.Id, CharacteristicId = characteristic.Id, Order = 1 };

      var houseCharacteristicsArray = new List<TypeCharacteristic>();
      houseCharacteristics.ToList().ForEach(item =>
      {
        var entity = new TypeCharacteristic
        {
          CharacteristicId = item.CharacteristicId,
          TypeId = item.TypeId,
          Order = item.Order
        };
        houseCharacteristicsArray.Add(entity);
      });

      context.TypeCharacteristics.AddOrUpdate(houseCharacteristicsArray.ToArray());

    }
  }
}
