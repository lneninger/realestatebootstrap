require.config({
    baseUrl: "libs",
    paths: {
        'angular': 'angularjs/angular.min',
        'angular-route': 'angularjs/angular-route.min',
        'angular-resource': 'angularjs/angular-resource.min',
        'angular-animate': 'angularjs/angular-animate.min',
        'angularAMD': '.../../angularAMD-0.2.1.min',
        'bootstrap': 'bootstrap/js',
        'common-tools': '.../../../app/common-tools',
        'app': '.../../../app/app',
        'app-commons': '.../../../app/commons',
        'app-controllers': '.../../../app/controllers',
        'templates': '.../../../templates',
        'jquery':'jquery/jquery-2.1.1.min',
        'jquery-validate':'jquery/jquery.validator/jquery.validate.min',
        'jquery-ui':'jquery-ui/jquery-ui.min',

    },
    shim: {'angular':['jquery'], 'angularAMD': ['angular'], 'angular-route': ['angular'], 'angular-resource': ['angular'], 'angular-animate': ['angular'], 'bootstrap/ui-bootstrap-tpls.min': ['angular'], 'toaster':['angular'], 'jquery-ui':['jquery'], 'bootstrap/bootstrap.min':['jquery', 'angular'], 'common-tools': ['jquery', 'jquery-validate'], 'jquery-validate': ['jquery'], 'app-commons/propertyService': ['app'], 'app-commons/propertyTypeService': ['app'], 'app-commons/propertyCharacteristicService': ['app'], 'app-commons/addressService': ['app'] },

    deps: ['jquery', 'jquery-ui', 'jquery-validate', 'linqInJavaScript', 'angular', 'app', 'app-commons/propertyDirective', 'app-commons/customSliderDirective', 'app-commons/propertyInlineCaroselDirective', 'app-commons/firstPictureFilter']
});
