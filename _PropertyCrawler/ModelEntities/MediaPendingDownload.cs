﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelEntities
{
  public class MediaPendingDownload : IObjectWithState
  {
    public Guid Id { get; set; }

    public string MediaUrl { get; set; }

    public string ThumbnailUrl { get; set; }

    public long PropertyId { get; set; }
    public Property Property { get; set; }
    public CustomEntityState EntityState { get; set; }

  }
}
