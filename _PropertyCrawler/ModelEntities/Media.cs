﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelEntities
{
  public class Media : IObjectWithState
  {
    public long PropertyId { get; set; }

    public Guid Id { get; set; }

    public string MediaTitle { get; set; }

    public string MediaUrl { get; set; }

    public string ThumbnailTitle { get; set; }

    public string ThumbnailUrl { get; set; }

    public bool Active { get; set; }

    public CustomEntityState EntityState { get; set; }

    public Property Property { get; set; }
  }
}
