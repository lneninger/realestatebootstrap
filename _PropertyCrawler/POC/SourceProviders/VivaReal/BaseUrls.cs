﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POC.SourceProviders.VivaReal
{
  public static class BaseUrls
  {
    public static string[] ListUrls = new string[]{
    "http://api.vivareal.com/api/1.0/locations/<state>/<city>/listings?hasImage=true&portal=VR_BR&page=<page>&maxResults=20&apiKey=183d98b9-fc81-4ef1-b841-7432c610b36e&language=pt&currency=BRL&business=VENTA"
    };

    public static string[] states = new string[] { "sp" };
    public static string[] cities = new string[] { "sao-paulo" };
  }
}
