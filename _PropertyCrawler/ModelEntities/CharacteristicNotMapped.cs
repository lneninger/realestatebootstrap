﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModelEntities
{
  public class CharacteristicNotMapped: Characteristic, IObjectWithState
  {
    public PropertySourcesEnum PropertySource { get; set; }

    public CustomEntityState EntityState { get; set; }
  }
}
