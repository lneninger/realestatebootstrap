define(['angularAMD'], function(angularAMD) {
  angularAMD.directive('customSlider', function($location, $compile) {
    var doLink = function(scope, element, attrs, controller) {


      element.slider({
        range: (scope.minValue == undefined || scope.maxValue == undefined) ? "min" : true,
        min: parseInt(scope.floor, 10),
        max: parseInt(scope.ceiling, 10),
        value:(scope.minValue == undefined || scope.maxValue == undefined) ? parseInt(scope.floor, 10) : undefined,
        values: (scope.minValue == undefined || scope.maxValue == undefined) ? undefined : [parseInt(scope.floor, 10), parseInt(scope.ceiling, 10)],
        slide: function(event, ui) {
          scope.$apply(function() {
            if (scope.minValue != undefined && scope.maxValue != undefined) {
              scope.minValue = ui.values[0];
              scope.maxValue = ui.values[1];
            }
            else
            {
              scope.currentValue = ui.value;
            }
          });
        }
      });


    };


    return {
      restrict: "E",
      replace: true,
      template: '<div></div>',
      link: doLink,
      scope: {
        floor: '@',
        ceiling: '@',
        minValue: '=?',
        maxValue: '=?',
        currentValue: '=?'
      }
    };
  });

});
