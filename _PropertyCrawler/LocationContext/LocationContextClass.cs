﻿using EntityFrameworkModeling;
using ModelEntities.Location;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocationContext
{
  public class LocationContextClass: BaseContext<LocationContextClass>
  {
    public const string SchemaName = "Locations";
   

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
      /*ZipCodes*/
      modelBuilder.Entity<ZipCode>()
      .ToTable("ZipCodes", SchemaName)
      .HasKey<Guid>(t => t.Id);
    }
  }
}
