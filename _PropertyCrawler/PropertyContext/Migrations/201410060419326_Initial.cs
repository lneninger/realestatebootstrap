namespace PropertyContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Properties.Categories",
                c => new
                    {
                        CD_Category = c.Int(nullable: false, identity: true),
                        NM_Identifier = c.String(maxLength: 64),
                    })
                .PrimaryKey(t => t.CD_Category);
            
            CreateTable(
                "Properties.Characteristics",
                c => new
                    {
                        CD_Characteristic = c.Int(nullable: false, identity: true),
                        NM_Identifier = c.String(maxLength: 64),
                        NM_DataType = c.String(maxLength: 64),
                        CD_Category = c.Int(nullable: false),
                        CD_PropertySource = c.Int(),
                        IN_Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.CD_Characteristic)
                .ForeignKey("Properties.Categories", t => t.CD_Category, cascadeDelete: true)
                .Index(t => t.CD_Category);
            
            CreateTable(
                "Properties.TypeCharacteristics",
                c => new
                    {
                        CD_TypeCharacteristic = c.Int(nullable: false, identity: true),
                        NR_Order = c.Int(nullable: false),
                        CD_Characteristic = c.Int(nullable: false),
                        CD_Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CD_TypeCharacteristic)
                .ForeignKey("Properties.Types", t => t.CD_Type, cascadeDelete: true)
                .ForeignKey("Properties.Characteristics", t => t.CD_Characteristic, cascadeDelete: true)
                .Index(t => t.CD_Characteristic)
                .Index(t => t.CD_Type);
            
            CreateTable(
                "Properties.Types",
                c => new
                    {
                        CD_Type = c.Int(nullable: false, identity: true),
                        NM_Identifier = c.String(maxLength: 2000),
                        Characteristic_Id = c.Int(),
                    })
                .PrimaryKey(t => t.CD_Type)
                .ForeignKey("Properties.Characteristics", t => t.Characteristic_Id)
                .Index(t => t.Characteristic_Id);
            
            CreateTable(
                "dbo.Cities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StateId = c.Int(nullable: false),
                        Identifier = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.States", t => t.StateId, cascadeDelete: true)
                .Index(t => t.StateId);
            
            CreateTable(
                "dbo.States",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Identifier = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Neighborhoods",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Properties.Properties",
                c => new
                    {
                        CD_Property = c.Long(nullable: false, identity: true),
                        CD_PropertySource = c.Int(nullable: false),
                        NM_ExternalId = c.String(maxLength: 2000),
                    })
                .PrimaryKey(t => t.CD_Property);
            
            CreateTable(
                "Properties.CharacteristicValues",
                c => new
                    {
                        CD_CharacteristicValue = c.Guid(nullable: false, identity: true),
                        CD_Property = c.Long(nullable: false),
                        DS_Value = c.String(),
                        CD_Characteristic1 = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CD_CharacteristicValue)
                .ForeignKey("Properties.Characteristics", t => t.CD_Characteristic1, cascadeDelete: true)
                .ForeignKey("Properties.Properties", t => t.CD_Property, cascadeDelete: true)
                .Index(t => t.CD_Property)
                .Index(t => t.CD_Characteristic1);
            
            CreateTable(
                "Properties.PropertyMedias",
                c => new
                    {
                        CD_Media = c.Guid(nullable: false, identity: true),
                        CD_Property = c.Long(nullable: false),
                        NM_MediaTitle = c.String(maxLength: 200),
                        DS_MediaUrl = c.String(maxLength: 2000),
                        NM_ThumbnailTitle = c.String(maxLength: 200),
                        DS_ThumbnailUrl = c.String(maxLength: 2000),
                        IN_Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.CD_Media)
                .ForeignKey("Properties.Properties", t => t.CD_Property, cascadeDelete: true)
                .Index(t => t.CD_Property);
            
            CreateTable(
                "Properties.ZipCodes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Pendings.PropertyMediaDownload",
                c => new
                    {
                        CD_Media = c.Guid(nullable: false, identity: true),
                        DS_MediaUrl = c.String(maxLength: 2000),
                        DS_ThumbnailUrl = c.String(maxLength: 2000),
                        CD_Property = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.CD_Media)
                .ForeignKey("Properties.Properties", t => t.CD_Property, cascadeDelete: true)
                .Index(t => t.CD_Property);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Pendings.PropertyMediaDownload", "CD_Property", "Properties.Properties");
            DropForeignKey("Properties.PropertyMedias", "CD_Property", "Properties.Properties");
            DropForeignKey("Properties.CharacteristicValues", "CD_Property", "Properties.Properties");
            DropForeignKey("Properties.CharacteristicValues", "CD_Characteristic1", "Properties.Characteristics");
            DropForeignKey("dbo.Cities", "StateId", "dbo.States");
            DropForeignKey("Properties.Characteristics", "CD_Category", "Properties.Categories");
            DropForeignKey("Properties.Types", "Characteristic_Id", "Properties.Characteristics");
            DropForeignKey("Properties.TypeCharacteristics", "CD_Characteristic", "Properties.Characteristics");
            DropForeignKey("Properties.TypeCharacteristics", "CD_Type", "Properties.Types");
            DropIndex("Pendings.PropertyMediaDownload", new[] { "CD_Property" });
            DropIndex("Properties.PropertyMedias", new[] { "CD_Property" });
            DropIndex("Properties.CharacteristicValues", new[] { "CD_Characteristic1" });
            DropIndex("Properties.CharacteristicValues", new[] { "CD_Property" });
            DropIndex("dbo.Cities", new[] { "StateId" });
            DropIndex("Properties.Types", new[] { "Characteristic_Id" });
            DropIndex("Properties.TypeCharacteristics", new[] { "CD_Type" });
            DropIndex("Properties.TypeCharacteristics", new[] { "CD_Characteristic" });
            DropIndex("Properties.Characteristics", new[] { "CD_Category" });
            DropTable("Pendings.PropertyMediaDownload");
            DropTable("Properties.ZipCodes");
            DropTable("Properties.PropertyMedias");
            DropTable("Properties.CharacteristicValues");
            DropTable("Properties.Properties");
            DropTable("dbo.Neighborhoods");
            DropTable("dbo.Countries");
            DropTable("dbo.States");
            DropTable("dbo.Cities");
            DropTable("Properties.Types");
            DropTable("Properties.TypeCharacteristics");
            DropTable("Properties.Characteristics");
            DropTable("Properties.Categories");
        }
    }
}
