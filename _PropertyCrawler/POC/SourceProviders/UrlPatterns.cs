﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POC.SourceProviders
{
  public static class UrlPatterns
  {

    public static string StartUrl = @"http(s)?://www\.vivareal\.com\.br(/(#)?)?$";

    public static string CityListUrl = @"http(s)?://www\.vivareal\.com\.br/(?<businesstype>[^/].+)/(?<state>[^/].+)/(?<city>[^/].+)(/(#)?)?$";

    public static string PropertyDetailUrl = @"http(s)?://www\.vivareal\.com\.br/.*-id-(?<id>\d*)";

    public static string EntrepreneurshipDetailUrl = @"http(s)?://www\.vivareal\.com\.br/.*-id-(?<id>\d*)";
    
    public static string Entrepreneurship2DetailUrl = @"http(s)?://www\.vivareal\.com\.br/.*(?<statusType>IMOVEIS-LANCAMENTO)/.*-(?<id>\d*)(/(#)?)?$";
  }
}
