﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelEntities
{
  public enum CustomEntityState
  {
    Unchanged = 1,
    Added = 2,
    Updated = 3,
    Deleted = 4
  }
}
