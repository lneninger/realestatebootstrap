﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace POC_JSON
{
  class Program
  {
    static void Main(string[] args)
    {
      var url = string.Empty;
      var counter = 0;
      var properties = new List<POC_Core.Models.Property>();
      for (var i = 0; i < BaseUrls.states.Length; i++)
      {
        for (var j = 0; j < BaseUrls.cities.Length; j++)
        {
          for (var k = 0; j < BaseUrls.ListUrls.Length; k++)
          {
            var finished = false;
            counter = 1;
            do
            {
              url = BaseUrls.ListUrls[k].Replace("<state>", BaseUrls.states[i]).Replace("<city>", BaseUrls.cities[j]).Replace("<page>", counter.ToString());
              JToken jsonObj = null;
              var stream = new MemoryStream();
              var request = System.Net.WebRequest.Create(url);
              using (var response = request.GetResponse())
              {
                response.GetResponseStream().CopyTo(stream);
              }
              var test = System.Text.Encoding.UTF8.GetString(stream.GetBuffer());

              try
              {
                jsonObj = JToken.Parse(test);
              }
              catch (Exception ex)
              {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                break;
              }

              var propertyArray = jsonObj.Value<JArray>("listings");
              for (var l = 0; l < propertyArray.Count; l++)
              {
                var propertyObj = propertyArray[l];
                var property = new POC_Core.Models.Property();
                propertyObj.Children().ToList().ForEach(item =>
                {
                  TokenParser(property, item);
                });
                properties.Add(property);
              }
              counter++;
              var test1 = test;
            }
            while (!finished);
          }
        }
      }
    }

    public static string[] externalNoCharacteristicNames = new string[] { "links", "propertyId", "image", "thumbnail", "thumbnails", "images", "developmentInformation", "additionalFeatures" };
    private static void TokenParser(POC_Core.Models.Property property, JToken item)
    {
      var jProperty = item as JProperty;
      if (externalNoCharacteristicNames.Contains(jProperty.Name))
      {
        property.SetValue(jProperty);
      }
      else
      {
        property.Attributes.Add(jProperty.Name, jProperty.Value.Value<string>());
      }
    }

    static void RequestCallback(IAsyncResult ar)
    {
      var b = 0;
    }
  }

  public static class PropertyExtensions
  {
    public static void SetValue(this POC_Core.Models.Property property, JProperty jProperty)
    {
      if (jProperty.Name == "propertyId")
      {
        property.InternalId = jProperty.Value.Value<string>();
      }

      if (jProperty.Name == "thumbnails")
      {
        var jArray = jProperty.Value.Value<JArray>();
        for (var i = 0; i < jArray.Count; i++)
        {
          var thumbnail = jArray[i];
          if (property.Thumbnails.Count <= i)
          {
            property.Thumbnails.Add(new POC_Core.Models.Media());
          }
          property.Thumbnails[i].ThumbnailTitle = null;
          property.Thumbnails[i].ThumbnailUrl = thumbnail.Value<string>();
        }

      }

      if (jProperty.Name == "images")
      {
        var jArray = jProperty.Value.Value<JArray>();
        for (var i = 0; i < jArray.Count; i++)
        {
          var jToken = jArray[i];
          if (property.Thumbnails.Count < i - 1)
          {
            property.Thumbnails.Add(new POC_Core.Models.Media());
          }
          property.Thumbnails[i].MediaTitle = null;
          property.Thumbnails[i].MediaUrl = jToken.Value<string>();
        }

      }

      if (jProperty.Name == "additionalFeatures")
      {
        var jArray = jProperty.Value.Value<JArray>();
        if (jArray == null)
          return;
        for (var i = 0; i < jArray.Count; i++)
        {
          var jToken = jArray[i];
          property.Attributes.Add(jToken.Value<string>(), true.ToString());
        }

      }

    }
  }
}