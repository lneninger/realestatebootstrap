﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace EntityFrameworkModeling.Repositories
{
  public interface IBaseRepository<T>: IDisposable
  {
    DbContext context { get; }
    T Find<TId>(TId id);
    void Delete<TId>(TId id);
    void InsertOrUpdate(params T[] entities);
  }
}
