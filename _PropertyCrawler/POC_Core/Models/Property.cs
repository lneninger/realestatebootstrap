﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POC_Core.Models
{
  public class Property
  {

    public Property()
    {
      Attributes = new Dictionary<string, string>();
      Thumbnails = new List<Media>();
    }

    /// <summary>
    /// Source based id for property
    /// </summary>
    public string InternalId { get; set; }

    /// <summary>
    /// Fonte da informação
    /// </summary>
    public string Source { get; set; }

    public string IdType { get; set; }

    public string InternalReference { get; set; }

    public List<Media> Thumbnails { get; set; }

    public bool Completed { get; set; }

    public Dictionary<string, string> Characteristics { get; set; }

    public Dictionary<string, string> Attributes { get; set; }
  }
}
