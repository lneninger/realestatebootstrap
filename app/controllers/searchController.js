define(['app', 'app-commons/propertyCharacteristicService', 'app-commons/propertyTypeService', 'app-commons/propertyService', 'app-commons/propertyCharacteristicCategoryService', 'app-commons/addressService'], function(myApp) {
  myApp.controller('searchController', ['$scope', 'propertyService', 'propertyTypeService', 'propertyCharacteristicService',
    function($scope, propertyService, propertyTypeService) {


      var self = this;
      $scope.showDetailsFlag = false;
      $scope.showDetailedFilter = false;
      $scope.filter = {
        minPrice: 0,
        maxPrice: 0,
        withImage: false,
      };

      $scope.selectedForDetails = null;

      $scope.showDetails = function(property) {
        $scope.showDetailsFlag = true;
        $scope.selectedForDetails = property;
      };

      $scope.initialSearch = function() {
        //TODO: Something with $scope.filter
        $scope.showDetailedFilter = true;
        $scope.showDetailsFlag = false;
      };

      var propertiesPromise = propertyService.query();
      propertiesPromise.$promise.then(function(propertiesResult) {
        var properties = propertiesResult;
        $scope.properties = properties;
      });

      var propertyTypesPromise = propertyTypeService.query();
      propertyTypesPromise.then(function(propertyTypesResult) {
        var propertyTypes = propertyTypesResult;
        $scope.propertyTypes = propertyTypes;
      });

      

    }
  ]);
});
