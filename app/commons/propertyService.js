define(['app', 'angular-resource'], function(myApp){
    myApp.factory('propertyService', function($resource) {
        return $resource('http://localhost:28748/api/Properties', {
            'query': {
                method: 'GET',
                isArray: true
            }
        });
    });
});
