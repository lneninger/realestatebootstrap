define(['angularAMD'], function(angularAMD) {
    angularAMD.directive('hmenu', function($location) {
        var doLink = function(scope, element, attrs, controller) {
          scope.isActive = function(route) {
            return route === $location.path();
          };
        };

        return {
          restrict: "E",
          replace: true,
          templateUrl: '.../../templates/directives/hmenu.html',
          link: doLink,
          scope: {

          }
        };
    });
});
