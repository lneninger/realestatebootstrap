﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace POC_Core.Helpers
{
  public class NetHelper
  {
    public static NetHelper CreateInstance()
    {
      return new NetHelper();
    }

    protected NetHelper() { }


    public enum RequestMethod
    {
      GET,
      POST,
      PUT
    }

    public NetResponse HttpRequest(RequestMethod method, string url, int timeOut)
    {
      return HttpRequest(method, url, null, null, String.Empty, timeOut);
    }
    public NetResponse HttpRequest(RequestMethod method, string url, string userName, string password, int timeOut)
    {
      return HttpRequest(method, url, userName, password, String.Empty, timeOut);
    }
    public NetResponse HttpRequest(RequestMethod method, string url, string userName, string password, IDictionary<string, string> pars, int timeOut)
    {
      if (pars != null && pars.Count > 0)
      {
        string parsString = String.Join("&", pars.Select(e => String.Format("{0}={1}", e.Key, e.Value)));
      }
      return HttpRequest(method, url, userName, password, pars, timeOut);
    }
    public NetResponse HttpRequest(RequestMethod method, string url, string userName, string password, string pars, int timeOutSecond)
    {
      return HttpRequest(method, Encoding.ASCII, url, userName, password, pars, timeOutSecond);
    }
    public NetResponse HttpRequest(RequestMethod method, Encoding postEncoding, string url, string userName, string password, string pars, int timeOutSecond)
    {
      return HttpRequest(method, Encoding.ASCII, url, userName, password, pars, timeOutSecond, HTTPFlags.None);
    }
    public NetResponse HttpRequest(RequestMethod method, Encoding postEncoding, string url, string userName, string password, string pars, int timeOutSecond, HTTPFlags flags)
    {
      return HttpRequest(method, postEncoding, url, "application/x-www-form-urlencoded", userName, password, pars, timeOutSecond, flags);
    }
    public NetResponse HttpRequest(RequestMethod method, Encoding postEncoding, string url, string contentType, string userName, string password, string pars, int timeOutSecond, HTTPFlags flags)
    {
      return HttpRequest(method, postEncoding, url, contentType, null, userName, password, pars, timeOutSecond, flags);
    }

    public NetResponse HttpRequest(RequestMethod method, Encoding postEncoding, string url, string contentType, string accepts, string userName, string password, string pars, int timeOutSecond, HTTPFlags flags)
    {
      try
      {
        if (method == RequestMethod.GET && pars != null && pars.Length > 0)
        {
          url = String.Format("{0}?{1}", url, pars);
        }
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        if (!String.IsNullOrWhiteSpace(userName))
          request.Credentials = new NetworkCredential(userName, password);
        request.Timeout = timeOutSecond * 1000;
        request.Method = method.ToString();

        if (accepts != null)
          request.Accept = accepts;

        if (method == RequestMethod.POST && pars != null && pars.Length > 0)
        {
          byte[] parsBytes = null;
          parsBytes = postEncoding.GetBytes(pars);
          Stream requestStream = null;
          BinaryWriter requestBinaryWriter = null;
          try
          {
            request.ContentType = contentType;
            request.ContentLength = parsBytes.Length;
            requestStream = request.GetRequestStream();

            requestBinaryWriter = new BinaryWriter(requestStream);
            requestBinaryWriter.Write(parsBytes);
            requestBinaryWriter.Flush();
          }
          catch (Exception ex)
          {
            System.Diagnostics.Debug.WriteLine("Framework.Utils.Net.HttpGet" + String.Format("{0}{1}{2}", ex.Message, Environment.NewLine, ex.StackTrace), "Error");
            throw;
          }
          finally
          {
            if (requestBinaryWriter != null)
              requestBinaryWriter.Close();

          }
        }
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        MemoryStream tempStream = null;
        try
        {
          Stream responseStream = response.GetResponseStream();

          tempStream = new MemoryStream();
          responseStream.CopyTo(tempStream);
          byte[] bytes = tempStream.ToArray();
          return new NetResponse { Bytes = bytes, Response = response };


        }
        catch (Exception ex)
        {
          System.Diagnostics.Debug.WriteLine("Framework.Utils.Net.HttpGet" + String.Format("{0}{1}{2}", ex.Message, Environment.NewLine, ex.StackTrace), "Error");
          throw;
        }
        finally
        {
          if (tempStream != null)
          {
            tempStream.Dispose();
            tempStream = null;
          }
        }
      }
      catch (Exception ex)
      {
        System.Diagnostics.Debug.WriteLine("Framework.Utils.Net.HttpGet" + String.Format("{0}{1}{2}", ex.Message, Environment.NewLine, ex.StackTrace), "Error");
        throw;
      }
    }

    public class NetResponse
    {
      public HttpWebResponse Response { get; set; }

      public byte[] Bytes { get; set; }
    }


    [Flags]
    public enum FTPFlags
    {
      None = 1,
      Passive = 2,
      UseBinary = 4
    }

    [Flags]
    public enum HTTPFlags
    {
      None = 1,//Non flaged
      XML = 2//Is xml based communication
    }
  }
}
