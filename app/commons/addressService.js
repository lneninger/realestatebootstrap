define(['app', 'angular-resource'], function(myApp){
    myApp.factory('addressService', function($resource) {
        return $resource('sources/location.json', {
            'query': {
                method: 'GET',
                isArray: true
            }
        });

    });
});
