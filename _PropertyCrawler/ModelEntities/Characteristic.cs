﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModelEntities
{
  public class Characteristic
  {
    public int Id { get; set; }

    public string Identifier { get; set; }

    public ICollection<ModelEntities.Type> Types { get; set; }

    public string DataType { get; set; }

    public ICollection<TypeCharacteristic> TypeCharacteristics { get; set; }

    public Category Category { get; set; }

    public int CategoryId { get; set; }
  }
}
