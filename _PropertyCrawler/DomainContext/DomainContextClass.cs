﻿using EntityFrameworkModeling;
using ModelEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainContext
{
  public class DomainContextClass : BaseContext<DomainContextClass>
  {
    public const string SchemaName = "PropertyDomains";
    

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
      /*Types*/
      modelBuilder.Entity<ModelEntities.Type>()
      .ToTable("Types", SchemaName)
      .HasKey<long>(t => t.Id);

      modelBuilder.Entity<ModelEntities.Type>()
      .Property(t => t.Id)
      .HasColumnName("CD_Type");

      modelBuilder.Entity<ModelEntities.Type>()
     .Property(t => t.Identifier)
     .HasColumnName("NM_Identifier")
     .HasMaxLength(2000);

      modelBuilder.Entity<ModelEntities.Type>()
      .HasMany(t => t.TypeCharacteristics)
      .WithRequired(t => t.Type)
      .HasForeignKey(t => t.TypeId);

      /*Characteristics*/
      modelBuilder.Entity<Characteristic>()
    .Map<Characteristic>(m => m.Requires("IN_Active").HasValue(true))
    .Map<CharacteristicNotMapped>(m => m.Requires("IN_Active").HasValue(false))
      .ToTable("Characteristics", SchemaName)
      .HasKey<long>(t => t.Id);

      modelBuilder.Entity<Characteristic>()
    .Property(t => t.Id)
    .HasColumnName("CD_Characteristic");

      modelBuilder.Entity<Characteristic>()
      .Property(t => t.Identifier)
      .HasColumnName("NM_Identifier")
      .HasMaxLength(64);

      modelBuilder.Entity<Characteristic>()
     .Property(t => t.DataType)
     .HasColumnName("NM_DataType")
     .HasMaxLength(64);

      modelBuilder.Entity<Characteristic>()
     .Property(t => t.CategoryId)
     .HasColumnName("CD_Category");

      modelBuilder.Entity<Characteristic>()
       .HasMany(t => t.TypeCharacteristics)
       .WithRequired(t => t.Characteristic)
       .HasForeignKey(t => t.CharacteristicId);


      modelBuilder.Entity<CharacteristicNotMapped>()
        .Property(t => t.PropertySource)
        .HasColumnName("CD_PropertySource");


      modelBuilder.Entity<CharacteristicNotMapped>()
      .Ignore(t => t.EntityState);



      /*Category*/
      modelBuilder.Entity<Category>()
      .ToTable("Categories", SchemaName)
      .HasKey<int>(t => t.Id);

      modelBuilder.Entity<Category>()
      .Property(t => t.Id)
      .HasColumnName("CD_Category");

      modelBuilder.Entity<Category>()
      .Property(t => t.Identifier)
      .HasColumnName("NM_Identifier")
      .HasMaxLength(64);

      modelBuilder.Entity<Category>()
       .HasMany(t => t.Characteristics)
       .WithRequired(t => t.Category)
       .HasForeignKey(t => t.CategoryId);

      /*TypeCharacteristic*/
      modelBuilder.Entity<TypeCharacteristic>()
      .ToTable("TypeCharacteristics", SchemaName)
      .HasKey<int>(t => t.Id);

      modelBuilder.Entity<TypeCharacteristic>()
       .Property<int>(t => t.Id)
       .HasColumnName("CD_TypeCharacteristic");

      modelBuilder.Entity<TypeCharacteristic>()
       .Property<int>(t => t.Order)
       .HasColumnName("NR_Order");

      modelBuilder.Entity<TypeCharacteristic>()
       .HasRequired(t => t.Characteristic);

      modelBuilder.Entity<TypeCharacteristic>()
       .Property(t => t.CharacteristicId)
       .HasColumnName("CD_Characteristic");

      modelBuilder.Entity<TypeCharacteristic>()
      .Property(t => t.TypeId)
      .HasColumnName("CD_Type");

      modelBuilder.Entity<TypeCharacteristic>()
       .HasRequired(t => t.Type)
       .WithMany(t => t.TypeCharacteristics)
       .HasForeignKey(t => t.TypeId);


      base.OnModelCreating(modelBuilder);
    }
  }
}
