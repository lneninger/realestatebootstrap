
define(['app', 'angular-resource'], function(myApp){
    myApp.factory('propertyCharacteristicService', function($resource, $q) {
        var resource = $resource('http://localhost:28748/api/Characteristics', {
            'query': {
                method: 'GET',
                isArray: true
            }
        });

        var asyncQuery = function() {
            var deferred = $q.defer();
            resource.query().then(function(response) {
                deferred.resolve(response);
            });
            return deferred.promise;
        };

        var query = function() {
            var deferred = $q.defer();
            resource.query().$promise.then(function(response) {
                deferred.resolve(response);
            });
            return deferred.promise;
        };

        return{
            query: query,
            asyncQuery: asyncQuery
        };

    });
});
