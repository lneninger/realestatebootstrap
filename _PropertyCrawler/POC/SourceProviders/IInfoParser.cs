﻿using ModelEntities;
using POC_Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace POC.SourceProviders
{
  public interface IInfoParser
  {
    #region Old parsers
    string Price(string content, out string currency);

    string IdFromListItem(string content, out string idType);

    string IdFromDetailLink(string content, out string idType);

    string DetailLink(string content);
    string DetailLinkListItem(string content);
    string RoomsFromLinkListItem(string content);
    string GaragesFromLinkListItem(string content);

    string BusinessTypeFromLinkListItem(string content);

    string BuildedAreaFromLinkListItem(string content);

    string TypeFromListItem(string link);

    string StartupFromListItem(string link);

    string DescriptionListItem(string outerHtml);

    string CharacteristicAreaContent(string outerHtml);

    string SuitesFromCharacteristicListItem(string characteristicsContent);

    string GaragesFromCharacteristicListItem(string characteristicsContent);

    string BathroomsFromCharacteristicListItem(string characteristicArea);

    string DetailInternalReference(WebBrowser browser);

    HtmlElement DetailAttributesContainer(WebBrowser browser);

    Dictionary<string, string> DetailAttributes(HtmlElement container);

    string DetailAttributesArea(HtmlElement container, out string areaUnit);

    string DetailAttributesRooms(HtmlElement container);

    string DetailAttributesBathrooms(HtmlElement container);

    string DetailAttributesGarages(HtmlElement container);

    string DetailDescription(WebBrowser webBrowser, out string title);

    List<POC_Core.Models.Media> DetailThumbnails(WebBrowser webBrowser);

    Dictionary<string, string> DetailCharacteristics(WebBrowser browser);
    string DetailCharacteristicConditionerAir(WebBrowser browser);
    string DetailCharacteristicPool(WebBrowser browser);
    string DetailCharacteristicInterphone(WebBrowser browser);
    #endregion

    List<CharacteristicValue> ConvertToCharacteristics(Dictionary<string, string> rawAttributes);
  }
}
