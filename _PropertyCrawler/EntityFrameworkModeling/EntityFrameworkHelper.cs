﻿using ModelEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Data.Entity
{
  public static class EntityFrameworkHelper
  {

    public static void SetCurrentStateToEntities(this DbContext context)
    {
      var entries = context.ChangeTracker.Entries<IObjectWithState>();
      foreach(var entry in entries)
      {
        var currentStatus = (entry.Entity as IObjectWithState).EntityState;
        entry.State = RetrieveEFState(currentStatus);
      }
    }

    public static void SetToUndefined(this DbContext context)
    {
      var entries = context.ChangeTracker.Entries<IObjectWithState>();
      foreach (var entry in entries)
      {
        (entry.Entity as IObjectWithState).EntityState = CustomEntityState.Unchanged;
      }
    }

    public static System.Data.Entity.EntityState RetrieveEFState(CustomEntityState status)
    {
      switch (status)
      {
        case CustomEntityState.Added: return System.Data.Entity.EntityState.Added;
        case CustomEntityState.Updated: return System.Data.Entity.EntityState.Modified;
        case CustomEntityState.Deleted: return System.Data.Entity.EntityState.Deleted;
        default: return System.Data.Entity.EntityState.Modified;
      }
    }
   }
}
