﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelEntities
{
  public enum CategoriesEnum
  {
    DESCRIPTION = 1,
    IDS = 2,
    PRICE = 3,
    AREA = 4,
    LEISURE = 5,
    OUTDOOR = 6,
    STRUCTURE = 7,
    FURNITURE = 8,
    LOCATION = 9,
    UNDEFINED = 10,
  }
}
