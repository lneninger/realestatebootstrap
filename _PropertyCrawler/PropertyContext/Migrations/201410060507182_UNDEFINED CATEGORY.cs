namespace PropertyContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UNDEFINEDCATEGORY : DbMigration
    {
        public override void Up()
        {
          Sql("INSERT INTO Properties.Categories VALUES ('UNDEFINED')");
        }
        
        public override void Down()
        {
        }
    }
}
