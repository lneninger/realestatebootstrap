﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelEntities
{
  public class Category
  {
    public int Id { get; set; }

    public string Identifier { get; set; }

    public ICollection<Characteristic> Characteristics { get; set; }
  }
}
