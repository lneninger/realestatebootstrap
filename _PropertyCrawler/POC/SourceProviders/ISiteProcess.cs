﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace POC.SourceProviders
{
  public interface ISiteProcess
  {
    bool Initialized { get; set; }
    bool Finished { get; set; }
    int ItemPos { get; }
    List<HtmlElement> Items { get; set; }
    void ProcessCall(ISiteProcessor siteProcessor, string url);
  }
}
